#version 330 core
layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

out vec3 VertexColor;
out vec2 TexCoord;

uniform mat4 mvp;

void main()
{
    gl_Position = mvp * vec4(pos, 1.0f);
	VertexColor = vec3(1.0f, 1.0f, 1.0f);
	TexCoord = texCoord;
}