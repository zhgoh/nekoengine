#version 330 core
in vec2 spriteUV;
in vec4 color;

out vec4 Color;

uniform sampler2D Sample0;

void main()
{
	Color = texture(Sample0, spriteUV);
	//Color = vec4(1.0, 1.0, 1.0, 1.0);
	//Color = vec4(spriteUV, 0.0f, 1.0f);
}