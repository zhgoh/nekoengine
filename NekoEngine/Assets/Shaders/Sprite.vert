#version 330 core
layout(location = 0) in vec3 Pos;
layout(location = 1) in vec2 SpriteUV;

out vec4 color;
out vec2 spriteUV;

uniform mat4 mvp;

void main()
{
	spriteUV = SpriteUV;
	//spriteUV = SpriteFrame.xy + (UV * SpriteFrame.zw); 
	gl_Position = mvp * vec4(Pos, 1.0f);
	color = gl_Position;
}