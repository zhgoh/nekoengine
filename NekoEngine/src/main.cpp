#include "pch.h"
#include "Test.h"

void InitConsole();
void InitWindow(int nCmdShow);
void Run();
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void InitRawInput();

static HINSTANCE hInst;
static HWND hWnd;
static wchar_t title[] = L"Basket Brawl";
static bool isActivated;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int nCmdShow)
{
#if _DEBUG
  // Enable run-time memory check for debug builds.
  _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
  //_CrtSetBreakAlloc(0);
#endif

  hInst = hInstance;
  InitConsole();
  InitWindow(nCmdShow);
  InitRawInput();
  Run();
  return 0;
}

static void InitConsole()
{
  AllocConsole();
  FILE *file;
  freopen_s(&file, "CONOUT$", "wt", stdout);
  SetConsoleTitle(L"Debug");
}

static void InitWindow(int nCmdShow)
{
  WNDCLASSEX windowClass = {};
  windowClass.cbSize = sizeof(WNDCLASSEX);
  windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
  windowClass.lpfnWndProc = WndProc;
  windowClass.hInstance = hInst;
  //windowClass.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON1));
  windowClass.hCursor = LoadCursor(hInst, IDC_ARROW);
  windowClass.lpszClassName = title;
  RegisterClassEx(&windowClass);

  RECT winRect = { 0, 0, GetWindowWidth(), GetWindowHeight() };
  AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, false);
  int width = winRect.right - winRect.left;
  int height = winRect.bottom - winRect.top;

  DWORD style = WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
  hWnd = CreateWindow(title,
                      title,
                      style,
                      CW_USEDEFAULT,
                      CW_USEDEFAULT,
                      width,
                      height,
                      NULL,
                      NULL,
                      hInst,
                      NULL);

  ShowWindow(hWnd, nCmdShow);
  UpdateWindow(hWnd);

  RendererInit(hWnd);
}

static float time()
{
  static i64 start = 0;
  static i64 frequency = 0;

  if (start == 0)
  {
    QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&start));
    QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*>(&frequency));
    return 0.0f;
  }

  i64 counter = 0;
  QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&counter));
  return static_cast<float>((counter - start) / static_cast<double>(frequency));
}

static void Run()
{
#ifdef _DEBUG
  Test();
#endif

  MSG msg = {};
  EngineStartUp();

  const auto dt = 0.01f;

  auto t = 0.0f;
  auto accumulator = 0.0f;
  auto currentTime = 0.0f;

  while (EngineIsRunning())
  {
    while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
    {
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }

    auto newTime = time();
    auto deltaTime = newTime - currentTime;

    if (deltaTime <= 0.0f)
      continue;

    currentTime = newTime;
    accumulator += deltaTime;

    while (accumulator >= dt)
    {
      EngineUpdate(dt);
      accumulator -= dt;
      t += dt;
    }

    RendererDraw(0.0f);

    if (GetAsyncKeyState(VK_ESCAPE))
    {
      EngineStop();
    }
  }

  EngineShutDown();
}

static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
  if (ImGui_WndProcHandler(hWnd, message, wParam, lParam))
    return 1;

  LRESULT result = 0;
  switch (message)
  {
  case WM_ACTIVATE:
  {
    isActivated = !(wParam == WA_INACTIVE);
  } break;

  case WM_DESTROY:
  case WM_CLOSE:
  {
    EngineStop();
  } break;

  case WM_INPUT:
  {
    if (isActivated)
    {
      char buffer[sizeof(RAWINPUT)] = {};
      auto size = sizeof(RAWINPUT);

      GetRawInputData(reinterpret_cast<HRAWINPUT>(lParam), RID_INPUT, buffer, &size, sizeof(RAWINPUTHEADER));

      // Extract keyboard raw input data
      auto rawInput = reinterpret_cast<RAWINPUT *>(buffer);

      if (rawInput->header.dwType == RIM_TYPEKEYBOARD)
      {
        auto const & rawKeyBoard = rawInput->data.keyboard;

        // Do something with the data here
        UINT flags = rawKeyBoard.Flags;
        UINT virtualKey = rawKeyBoard.VKey;
        UINT scanCode = rawKeyBoard.MakeCode;

        if (virtualKey == 255)
        {
          // Discard "Fake keys" which are part of the escape sequence
          return 0;
        }

        if (virtualKey == VK_SHIFT)
        {
          // Correct left/right hand shift
          virtualKey = MapVirtualKey(scanCode, MAPVK_VSC_TO_VK_EX);
        }
        else if (virtualKey == VK_NUMLOCK)
        {
          // Correct Pause/Break and Num Lock, and set the extended bit
          //scanCode = (MapVirtualKey(virtualKey, MAPVK_VK_TO_VSC) | 0x100);
        }

        // e0 and e1 are escape sequence used for certain special keys such as print/pause break
        const auto isE0 = ((flags & RI_KEY_E0) != 0);
        const auto isE1 = ((flags & RI_KEY_E1) != 0);

        if (isE1)
        {
          // For escape sequence, turn the vk into the correct scan code using MapVirtualKey
          // However MapVirtualKey is unable to map VK_PAUSE ( this is a known bug), have map it manually
          //scanCode = (virtualKey == VK_PAUSE) ? 0x45 : MapVirtualKey(virtualKey, MAPVK_VK_TO_VSC);
        }

        switch (virtualKey)
        {
          // Right control and alt have their E0 bit set
        case VK_CONTROL:
        {
          virtualKey = isE0 ? VK_RCONTROL : VK_LCONTROL;
        } break;

        case VK_MENU:
        {
          virtualKey = isE0 ? VK_RMENU : VK_LMENU;
        } break;

        // NUMPAD ENTER has its e0 bit set
        case VK_RETURN:
        {
          if (isE0)
          {
            virtualKey = VK_SEPARATOR;
          }
        } break;

        // the standard INSERT, DELETE, HOME, END, PRIOR and NEXT keys will always have their e0 bit set, but the
        // corresponding keys on the NUMPAD will not.
        case VK_INSERT:
        {
          if (!isE0)
          {
            virtualKey = VK_NUMPAD0;
          }
        } break;

        case VK_DELETE:
        {
          if (!isE0)
          {
            virtualKey = VK_DECIMAL;
          }
        } break;

        case VK_HOME:
        {
          if (!isE0)
          {
            virtualKey = VK_NUMPAD7;
          }
        } break;

        case VK_END:
        {
          if (!isE0)
          {
            virtualKey = VK_NUMPAD1;
          }
        } break;

        case VK_PRIOR:
        {
          if (!isE0)
          {
            virtualKey = VK_NUMPAD9;
          }
        } break;

        case VK_NEXT:
        {
          if (!isE0)
          {
            virtualKey = VK_NUMPAD3;
          }
        } break;

        // The standard arrow keys will always have their e0 bit set, but the
        // corresponding keys on the NUMPAD will not.
        case VK_LEFT:
        {
          if (!isE0)
          {
            virtualKey = VK_NUMPAD4;
          }
        } break;

        case VK_RIGHT:
        {
          if (!isE0)
          {
            virtualKey = VK_NUMPAD6;
          }
        } break;

        case VK_UP:
        {
          if (!isE0)
          {
            virtualKey = VK_NUMPAD8;
          }
        } break;

        case VK_DOWN:
        {
          if (!isE0)
          {
            virtualKey = VK_NUMPAD2;
          }
        } break;

        // NUMPAD 5 doesn't have its e0 bit set
        case VK_CLEAR:
        {
          if (!isE0)
          {
            virtualKey = VK_NUMPAD5;
          }
        } break;

        default:
          break;
        }

        // A key can either produce a "make"/"break" scancode, this is to diff between down/release
        auto const isDown = ((flags & RI_KEY_BREAK) == 0);

        if (isDown)
        {
          // Send key down events to input system
          Input::KeyDownEvent(virtualKey);
        }
        else
        {
          // Send key up events to input system
          Input::KeyUpEvent(virtualKey);
        }
      }
      else if (rawInput->header.dwType == RIM_TYPEMOUSE)
      {
        auto &mouse = rawInput->data.mouse;
        Input::MouseEvent(mouse.usButtonFlags);
      }
    }
  } break;

  case WM_MOUSEMOVE:
  {
    Input::SetMousePos(static_cast<short>(lParam), static_cast<short>(lParam >> 16));
  } break;

  case WM_SIZE:
  {
    if (EngineIsRunning())
    {
      auto width = static_cast<i32>(LOWORD(lParam));
      auto height = static_cast<i32>(HIWORD(lParam));
      SetWindowWidth(width);
      SetWindowHeight(height);
      RendererResize(width, height);
    }
  } break;
  default:
  {
    result = DefWindowProc(hWnd, message, wParam, lParam);
  } break;
  }
  return result;
}

// Initializes RawInput after windows has set up
static void InitRawInput()
{
  const unsigned numDevice = 2;
  RAWINPUTDEVICE devices[numDevice];

  devices[0].usUsagePage = HID_USAGE_PAGE_GENERIC;
  devices[0].usUsage = HID_USAGE_GENERIC_MOUSE;
  devices[0].dwFlags = RIDEV_INPUTSINK;
  devices[0].hwndTarget = hWnd;

  devices[1].usUsagePage = HID_USAGE_PAGE_GENERIC;
  devices[1].usUsage = HID_USAGE_GENERIC_KEYBOARD;
  devices[1].dwFlags = RIDEV_INPUTSINK;
  devices[1].hwndTarget = hWnd;

  if (!RegisterRawInputDevices(devices, numDevice, sizeof devices[0]))
  {
    Log("RawInput failed with error code: ", GetLastError());
    ErrorIf(true);
  }
}
