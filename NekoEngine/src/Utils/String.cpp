#include "pch.h"

errno_t ne_strcpy(char *dst, size_t sz, const char *src)
{
  return strcpy_s(dst, sz, src);
}