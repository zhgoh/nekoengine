#pragma once

void IDInit();
u32  IDGen();
void IDUse(u32 id); 
void IDReuse(u32 id);