#pragma once
#include <iostream>

struct Logger
{
  void operator()(bool val) const
  {
#if _DEBUG
    std::cout << std::boolalpha << val << std::endl;
#endif
  }

	template<typename T>
	void operator()(T &&val)
	{
#if _DEBUG
		std::cout << std::forward<T>(val) << std::endl;
#endif
	}

	template<typename T, typename ... Args>
	void operator()(T &&val, Args && ... args)
	{
#if _DEBUG
		std::cout << std::forward<T>(val) << " ";
		(*this)(std::forward<Args>(args)...);
#endif
	}
};

template<typename ... Args>
void Print(Args && ... args)
{
	Logger log;
	log(std::forward<Args>(args)...);
}

template<typename ... Args>
void Log(Args && ... args)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),
							FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	Print("[Log]", std::forward<Args>(args)...);
}

template<typename ... Args>
void LogWarning(Args && ... args)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),
							FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	Print("[Warning]", std::forward<Args>(args)...);
}

template<typename ... Args>
void LogError(Args && ... args)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),
							FOREGROUND_RED | FOREGROUND_INTENSITY);
	Print("[Error]", std::forward<Args>(args)...);
}

#define ErrorIf(expr, ...)		\
do								            \
{								              \
	if (expr)					          \
	{							              \
		LogError(__VA_ARGS__);	  \
		__debugbreak();			      \
	}							              \
} while(0)