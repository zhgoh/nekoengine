#include  "pch.h"

INTERNAL Array<u32, MAX_ENTITY> ids;

void IDInit()
{
  u32 i = 0;
  ids.resize(MAX_ENTITY);

  for (auto &elem : ids)
  {
    elem = i++;
  }
}

u32 IDGen()
{
  auto lastID = ids.last();
  ids.pop();
  return lastID;
}

void IDUse(u32 id)
{
  ids.remove(id);
}

void IDReuse(u32 id)
{
  ids.push(id);
}
