#pragma once

int GetWindowWidth();
int GetWindowHeight();

void SetWindowWidth(i32 width);
void SetWindowHeight(i32 height);