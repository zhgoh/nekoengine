#include "pch.h"
#include "ThreadPool.h"

ThreadPool::ThreadPool(size_t numThds) :
  done(false)
{
  for (size_t i = 0; i < numThds; ++i)
  {
    workers.emplace_back(
      [this]
    {
      while (true)
      {
        std::function<void()> task;
        {
          std::unique_lock<std::mutex> lock(queueMutex);

          // If there are none, wait for notification
          cv.wait(lock, [this] { return done || !tasks.empty(); });

          // Exit if pool is stopped
          if (done && tasks.empty())
            return;

          // Otherwise get new task from queue
          task = move(tasks.front());
          tasks.pop();
        }

        // Execute the task
        task();
      }
    });
  }
}

ThreadPool::~ThreadPool()
{
  {
    std::unique_lock<std::mutex> lock(queueMutex);

    // Stop all threads
    done = true;
  }
  
  cv.notify_all();

  // Join them
  for (auto &elem : workers)
  {
    elem.join();
  }
}
