#include "pch.h"

i32 Input::keyCode = -1;
i32 Input::lastKeyPressed = -1;
i32 Input::currentKeyPressed = -1;

std::array<bool, 256> Input::keys;
std::array<bool, 256> Input::keyReleased;
std::array<bool, 256> Input::keysPrevious;

std::unordered_map<std::string, i32> Input::keyBindings;

bool Input::leftMouseDown = false;
bool Input::leftMousePressed = false;
bool Input::leftMouseReleased = false;

bool Input::rightMouseDown = false;
bool Input::rightMousePressed = false;
bool Input::rightMouseReleased = false;

i32 Input::mouseX = 0;
i32 Input::mouseY = 0;
i32 Input::prevMouseX = 0;
i32 Input::prevMouseY = 0;

void Input::Update()
{
  leftMousePressed = false;
  leftMouseReleased = false;

  rightMousePressed = false;
  rightMouseReleased = false;

  lastKeyPressed = currentKeyPressed;

  prevMouseX = mouseX;
  prevMouseY = mouseY;
}

void Input::KeyDownEvent(i32 key)
{
  currentKeyPressed = key;
  keysPrevious[key] = keys[key];
  keys[key] = true;
  keyCode = key;
}

void Input::KeyUpEvent(i32 key)
{
  currentKeyPressed = -1;
  lastKeyPressed = -1;
  keysPrevious[key] = keys[key];
  keyReleased[key] = true;
  keys[key] = false;
}

void Input::MouseEvent(i32 button)
{
  switch (button)
  {
    case RI_MOUSE_LEFT_BUTTON_DOWN:
    {
      leftMouseDown = true;
      leftMousePressed = true;
      leftMouseReleased = false;
    } break;

    case RI_MOUSE_LEFT_BUTTON_UP:
    {
      leftMouseDown = false;
      leftMousePressed = false;
      leftMouseReleased = true;
    } break;

    case RI_MOUSE_MIDDLE_BUTTON_DOWN:
    {
    } break;

    case RI_MOUSE_MIDDLE_BUTTON_UP:
    {
    } break;

    case RI_MOUSE_RIGHT_BUTTON_DOWN:
    {
      rightMouseDown = true;
      rightMousePressed = true;
      rightMouseReleased = false;
    } break;

    case RI_MOUSE_RIGHT_BUTTON_UP:
    {
      rightMouseDown = false;
      rightMousePressed = false;
      rightMouseReleased = true;
    } break;
    default:
      break;
  }
}

bool Input::KeyDown(i32 key)
{
  return keys[key];
}

bool Input::KeyPressed(i32 key)
{
  if (!keysPrevious[key] && keys[key])
  {
    keysPrevious[key] = keys[key];
    return true;
  }
  return false;

}

bool Input::KeyReleased(i32 key)
{
  if (keyReleased[key])
  {
    keyReleased[key] = false;
    return true;
  }
  return false;
}

bool Input::KeyDown(cstr keyString)
{
  return KeyDown(GetKeyString(keyString));
}

bool Input::KeyPressed(cstr keyString)
{
  return KeyPressed(GetKeyString(keyString));
}

bool Input::KeyReleased(cstr keyString)
{
  return KeyReleased(GetKeyString(keyString));
}

bool Input::IsMouseMoving()
{
  return (mouseX != prevMouseX)
    && (mouseY != prevMouseY);
}

bool Input::AnyKeyDown()
{
  for (const auto &elem : keys)
  {
    if (elem)
    {
      return elem;
    }
  }
  return false;
}

bool Input::MouseDown(i32 button)
{
  switch (button)
  {
    case Button::LEFTMOUSE:
    {
      return leftMouseDown;
    };

    case Button::RIGHTMOUSE:
    {
      return rightMouseDown;
    };

    case Button::MIDDLEMOUSE:
    {
    } break;
    default:
      break;
  }
  return false;
}

bool Input::MousePressed(i32 button)
{
  switch (button)
  {
    case Button::LEFTMOUSE:
    {
      return leftMousePressed;
    };

    case Button::RIGHTMOUSE:
    {
      return rightMousePressed;
    };

    case Button::MIDDLEMOUSE:
    {
    } break;
    default:
      break;
  }
  return false;
}

bool Input::MouseReleased(i32 button)
{
  switch (button)
  {
    case Button::LEFTMOUSE:
    {
      return leftMouseReleased;
    };

    case Button::RIGHTMOUSE:
    {
      return rightMouseReleased;
    };

    case Button::MIDDLEMOUSE:
    {
    } break;
    default:
      break;
  }
  return false;
}

void Input::SetMousePos(i32 x, i32 y)
{
  mouseX = x;
  mouseY = y;
}

i32 Input::GetMouseX()
{
  return mouseX;
}

i32 Input::GetMouseY()
{
  return mouseY;
}

i32 Input::GetKeyCode()
{
  return keyCode;
}

i32 Input::GetKeyString(const std::string &keyStr)
{
  ErrorIf(keyBindings.find(keyStr) == keyBindings.end(), "No key found.");
  return keyBindings[keyStr];
}

void Input::SetKeyString(const std::string &keyStr, i32 key)
{
  keyBindings[keyStr] = key;
}