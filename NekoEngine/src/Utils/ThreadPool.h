#pragma once

// https://github.com/progschj/ThreadPool/

/*
 * Threadpool using locked queue and threads
 */
class ThreadPool
{
public:
  explicit ThreadPool(size_t numThds);
  ~ThreadPool();

  template<typename F, typename ... Args>
  auto Enqueue(F &&f, Args && ... args) -> 
    std::future<typename std::result_of<F(Args ...)>::type>;

private:
  bool done;
  std::mutex queueMutex;
  std::condition_variable cv;
  
  std::vector<std::thread> workers;
  std::queue<std::function<void()>> tasks;
};

template<typename F, typename ...Args>
auto ThreadPool::Enqueue(F &&f, Args && ...args) -> 
  std::future<typename std::result_of<F(Args ...)>::type>
{
  using namespace std;
  using ret = typename result_of<F(Args ...)>::type;

  auto task = move(make_shared<packaged_task<ret()>>(
    bind(forward<F>(f), forward<Args>(args)...)
    ));

  future<ret> res = task->get_future();
  {
    unique_lock<mutex> lock(queueMutex);

    // Don't allow enq after stopping
    if (done) 
      throw runtime_error("Enqueue on stop");

    tasks.emplace([task]() { (*task)(); });
  }
  cv.notify_one();
  return res;
}
