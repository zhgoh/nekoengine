#pragma once

template<typename T, size_t sz>
class Array
{
public:
  Array();

  Array(const Array &rhs);
  Array &operator=(const Array &rhs);

  Array(Array &&rhs) noexcept;
  Array &operator=(Array &&rhs) noexcept;

  template<typename U>
  void    push(U &&elem);
  void    pop();
  void    increment();
  int     remove(const T &elem);
  void    removeAt(size_t i);
  void    shift(size_t i, size_t num = 1);
  void    resize(size_t newSize);
  void    clear();
  int     find(const T &elem);

  T       &last();
  const T *data() const;
  size_t  size() const;

  T       &operator[](size_t index);
  const T &operator[](size_t index) const;

  T       *begin();
  const T *begin() const;
  const T *cbegin();
  T       *end();
  const T *end() const;
  const T *cend();

private:
  size_t numOfElems;
  T arr[sz];
};

template<typename T, size_t sz>
Array<T, sz>::Array()
  : numOfElems{ 0 }
{
}

template<typename T, size_t sz>
Array<T, sz>::Array(const Array &rhs) :
  numOfElems(rhs.numOfElems)
{
  assert(numOfElems < sz);
  assert(rhs.numOfElems < sz);

  for (auto i = 0; i < sz; ++i)
  {
    const auto &elem = rhs.arr[i];
    arr[i] = elem;
  }
}

template<typename T, size_t sz>
Array<T, sz> &Array<T, sz>::operator=(const Array &rhs)
{
  assert(numOfElems < sz);
  assert(rhs.numOfElems < sz);

  Array<T, sz> tmp(rhs);
  std::swap(numOfElems, tmp.numOfElems);
  std::swap(arr, tmp.arr);

  return *this;
}

template<typename T, size_t sz>
Array<T, sz>::Array(Array &&rhs) noexcept : 
  numOfElems(0)
{
  assert(numOfElems < sz);
  assert(rhs.numOfElems < sz);

  std::swap(numOfElems, rhs.numOfElems);
  std::swap(arr, rhs.arr);
}

template <typename T, size_t sz>
Array<T, sz> & Array<T, sz>::operator=(Array &&rhs) noexcept
{
  assert(numOfElems < sz);
  assert(rhs.numOfElems < sz);

  std::swap(numOfElems, rhs.numOfElems);
  std::swap(arr, rhs.arr);

  return *this;
}

template<typename T, size_t sz>
template<typename U>
void Array<T, sz>::push(U &&elem)
{
  ErrorIf((numOfElems + 1) > sz, "Pushed too many elems");
  arr[numOfElems++] = std::forward<U>(elem);
}

template<typename T, size_t sz>
void Array<T, sz>::pop()
{
  ErrorIf((numOfElems - 1) < 0, "Popped too many elems");
  --numOfElems;
}

template<typename T, size_t sz>
T & Array<T, sz>::last()
{
  ErrorIf((numOfElems - 1) < 0, "Can't read last elem");
  return arr[numOfElems - 1];
}

template<typename T, size_t sz>
int Array<T, sz>::remove(const T &elem)
{
  auto id = find(elem);
  removeAt(id);
  return id;
}
template<typename T, size_t sz>
void Array<T, sz>::removeAt(size_t id)
{
  std::swap(arr[id], arr[numOfElems - 1]);
  pop();
}

template <typename T, size_t sz>
void Array<T, sz>::shift(size_t pos, size_t num)
{
  memcpy(arr + pos, arr + pos + num, numOfElems - (pos + num));
  numOfElems -= num;
}

template<typename T, size_t sz>
size_t Array<T, sz>::size() const
{
  return numOfElems;
}

template<typename T, size_t sz>
void Array<T, sz>::increment()
{
  ErrorIf(numOfElems + 1 > sz, "Pushed too many elems");
  ++numOfElems;
}

template<typename T, size_t sz>
T &Array<T, sz>::operator[](size_t index)
{
  return const_cast<T &>(static_cast<const Array<T, sz> &>(*this)[index]);
}

template<typename T, size_t sz>
const T &Array<T, sz>::operator[](size_t index) const
{
  ErrorIf(index > numOfElems, "Accessed out of range");
  ErrorIf(index > sz, "Accessed out of range");
  return arr[index];
}

template<typename T, size_t sz>
T *Array<T, sz>::begin()
{
  return &arr[0];
}

template<typename T, size_t sz>
const T *Array<T, sz>::begin() const
{
  return  &arr[0];
}

template<typename T, size_t sz>
const T *Array<T, sz>::cbegin()
{
  return  &arr[0];
}

template<typename T, size_t sz>
T *Array<T, sz>::end()
{
  return &arr[numOfElems];
}

template<typename T, size_t sz>
const T *Array<T, sz>::end() const
{
  return &arr[numOfElems];
}

template<typename T, size_t sz>
const T *Array<T, sz>::cend()
{
  return &arr[numOfElems];
}

template <typename T, size_t sz>
void Array<T, sz>::clear()
{
  numOfElems = 0;
}

template <typename T, size_t sz>
int Array<T, sz>::find(const T &elem)
{
  auto id = -1;
  for (T &e : arr)
  {
    ++id;
    if (e == elem)
    {
      break;
    }
  }
  return id;
}

template <typename T, size_t sz>
void Array<T, sz>::resize(size_t newSize)
{
  numOfElems = newSize;
}

template <typename T, size_t sz>
const T *Array<T, sz>::data() const
{
  return begin();
}
