#pragma once

// Raw Input stuffs
#ifndef HID_USAGE_PAGE_GENERIC
#define HID_USAGE_PAGE_GENERIC         ((USHORT) 0x01)
#endif

#ifndef HID_USAGE_GENERIC_MOUSE
#define HID_USAGE_GENERIC_MOUSE        ((USHORT) 0x02)
#endif

#ifndef HID_USAGE_GENERIC_KEYBOARD
#define HID_USAGE_GENERIC_KEYBOARD     ((USHORT) 0x06)
#endif

// Raw Input stuffs
#ifndef HID_USAGE_PAGE_GENERIC
#define HID_USAGE_PAGE_GENERIC         ((USHORT) 0x01)
#endif

#ifndef HID_USAGE_GENERIC_MOUSE
#define HID_USAGE_GENERIC_MOUSE        ((USHORT) 0x02)
#endif

#ifndef HID_USAGE_GENERIC_KEYBOARD
#define HID_USAGE_GENERIC_KEYBOARD     ((USHORT) 0x06)
#endif

class Input final
{
public:
  static void Update();

  static void KeyUpEvent(i32 key);
  static void KeyDownEvent(i32 key);
  static void MouseEvent(i32 key);

  static bool KeyDown(i32 key);
  static bool KeyPressed(i32 key);
  static bool KeyReleased(i32 key);

  static bool KeyDown(cstr keyString);
  static bool KeyPressed(cstr keyString);
  static bool KeyReleased(cstr keyString);

  static bool AnyKeyDown();

  static bool MouseDown(i32 button);
  static bool MousePressed(i32 button);
  static bool MouseReleased(i32 button);

  static i32 GetKeyCode();

  static i32 GetMouseX();
  static i32 GetMouseY();

  static bool IsMouseMoving();

  static void SetMousePos(i32 x, i32 y);
  static void SetKeyString(const std::string &name, i32 key);

  template<u32 N>
  static void GetKeyName(i32 key, char(&buffer)[N]);

private:
  static std::array<bool, 256> keysPrevious;
  static std::array<bool, 256> keys;
  static std::array<bool, 256> keyReleased;
  static std::unordered_map<std::string, i32> keyBindings;

  static i32 mouseX;
  static i32 mouseY;
  static i32 prevMouseX;
  static i32 prevMouseY;

  static i32 keyCode;

  static i32 lastKeyPressed;
  static i32 currentKeyPressed;

  static bool leftMouseDown;
  static bool leftMousePressed;
  static bool leftMouseReleased;

  static bool rightMouseDown;
  static bool rightMousePressed;
  static bool rightMouseReleased;

  static i32 GetKeyString(const std::string &);
};

template<u32 N>
void Input::GetKeyName(i32 key, char(&buffer)[N])
{
  auto scanCode = (MapVirtualKey(static_cast<UINT>(key), MAPVK_VK_TO_VSC) << 16);
  GetKeyNameTextA(static_cast<LONG>(scanCode), buffer, N);
}

//all mouse inputs
struct Button final
{
  static const u8 LEFTMOUSE = 0x01;
  static const u8 RIGHTMOUSE = 0x02;
  static const u8 MIDDLEMOUSE = 0x04;
};

//all keyboard inputs
struct Key final
{
  static const u8 BACKSPACE = 0x08;
  static const u8 TAB = 0x09;
  static const u8 ENTER = 0x0D;
  static const u8 LSHIFT = 0xA0;
  static const u8 RSHIFT = 0xA1;
  static const u8 LCTRL = 0xA2;
  static const u8 RCTRL = 0xA3;
  static const u8 LALT = 0xA4;
  static const u8 RALT = 0xA5;
  static const u8 PRi32SCREEN = 0x2C;
  static const u8 SCROLLLOCK = 0x91;
  static const u8 PAUSE = 0x13;
  static const u8 CAPSLOCK = 0x14;

  static const u8 ESCAPE = 0x1B;

  static const u8 SPACE = 0x20;
  static const u8 PAGEUP = 0x21;
  static const u8 PAGEDOWN = 0x22;
  static const u8 END = 0x23;
  static const u8 HOME = 0x24;
  static const u8 LEFT = 0x25;
  static const u8 UP = 0x26;
  static const u8 RIGHT = 0x27;
  static const u8 DOWN = 0x28;
  static const u8 INSERT = 0x2D;
  static const u8 DEL = 0x2E;

  static const u8 Key_0 = 0x30;
  static const u8 Key_1 = 0x31;
  static const u8 Key_2 = 0x32;
  static const u8 Key_3 = 0x33;
  static const u8 Key_4 = 0x34;
  static const u8 Key_5 = 0x35;
  static const u8 Key_6 = 0x36;
  static const u8 Key_7 = 0x37;
  static const u8 Key_8 = 0x38;
  static const u8 Key_9 = 0x39;

  static const u8 A = 0x41;
  static const u8 B = 0x42;
  static const u8 C = 0x43;
  static const u8 D = 0x44;
  static const u8 E = 0x45;
  static const u8 F = 0x46;
  static const u8 G = 0x47;
  static const u8 H = 0x48;
  static const u8 I = 0x49;
  static const u8 J = 0x4A;
  static const u8 K = 0x4B;
  static const u8 L = 0x4C;
  static const u8 M = 0x4D;
  static const u8 N = 0x4E;
  static const u8 O = 0x4F;
  static const u8 P = 0x50;
  static const u8 Q = 0x51;
  static const u8 R = 0x52;
  static const u8 S = 0x53;
  static const u8 T = 0x54;
  static const u8 U = 0x55;
  static const u8 V = 0x56;
  static const u8 W = 0x57;
  static const u8 X = 0x58;
  static const u8 Y = 0x59;
  static const u8 Z = 0x5A;

  static const u8 NUMPAD0 = 0x60;
  static const u8 NUMPAD1 = 0x61;
  static const u8 NUMPAD2 = 0x62;
  static const u8 NUMPAD3 = 0x63;
  static const u8 NUMPAD4 = 0x64;
  static const u8 NUMPAD5 = 0x65;
  static const u8 NUMPAD6 = 0x66;
  static const u8 NUMPAD7 = 0x67;
  static const u8 NUMPAD8 = 0x68;
  static const u8 NUMPAD9 = 0x69;

  static const u8 NUM_MULTIPLY = 0x6A;
  static const u8 NUM_PLUS = 0x6B;
  static const u8 NUM_ENTER = 0x6C;
  static const u8 NUM_MINUS = 0x6D;
  static const u8 NUM_PERIOD = 0x6E;
  static const u8 NUM_DIVIDE = 0x6F;
  static const u8 NUMLOCK = 0x90;

  static const u8 F1 = 0x70;
  static const u8 F2 = 0x71;
  static const u8 F3 = 0x72;
  static const u8 F4 = 0x73;
  static const u8 F5 = 0x74;
  static const u8 F6 = 0x75;
  static const u8 F7 = 0x76;
  static const u8 F8 = 0x77;
  static const u8 F9 = 0x78;
  static const u8 F10 = 0x79;
  static const u8 F11 = 0x7A;
  static const u8 F12 = 0x7B;

  static const u8 SEMICOLON = 0xBA;
  static const u8 SLASH = 0xBF;
  static const u8 BACKQUOTE = 0xC0;
  static const u8 LBRACKET = 0xDB;
  static const u8 BACKSLASH = 0XDC;
  static const u8 RBRACKET = 0xDD;
  static const u8 QUOTE = 0xDE;

  static const u8 EQUAL = 0xBB;
  static const u8 MINUS = 0xBD;
  static const u8 PERIOD = 0xBE;
  static const u8 COMMA = 0xBC;
};