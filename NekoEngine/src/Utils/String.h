#pragma once

errno_t ne_strcpy(char *dst, size_t sz, const char *src);

template<typename ... Args>
errno_t ne_strcpy(char *dst, size_t sz, const char *src, Args ... args)
{
  auto err = strcpy_s(dst, sz, src);
  return err ? err : ne_strcpy(dst + strlen(src), sz - strlen(dst), args...);
}

template<size_t n, typename ... Args>
errno_t ne_strcpy(char (&dst)[n], const char *src, Args ... args)
{
  return ne_strcpy(dst, n, src, args...);
}