#include "pch.h"

static i32 windowWidth = 1200;
static i32 windowHeight = 600;

i32 GetWindowWidth()
{
  return windowWidth;
}

i32 GetWindowHeight()
{
  return windowHeight;
}

void SetWindowWidth(i32 width)
{
  windowWidth = width;
}

void SetWindowHeight(i32 height)
{
  windowHeight = height;
}