#pragma once

namespace Player
{
  void Init(Handle hdl);
  void Begin();
  void Update();
  void End();
}
