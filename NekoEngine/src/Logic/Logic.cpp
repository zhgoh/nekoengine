#include "pch.h"
#include "Logic.h"
#include "Scripts.h"

struct Logic
{
  Array<VoidFn, MAX_LOGIC> beginFn;
  Array<VoidFn, MAX_LOGIC> updateFn;
  Array<VoidFn, MAX_LOGIC> endFn;
  Array<Handle, MAX_ENTITY> entityHdl;
};

INTERNAL u32                  numObjects = 0;
INTERNAL Logic                logic;

void LogicAdd(Handle hdl, InitFn initFn, VoidFn beginFn, VoidFn updateFn, VoidFn endFn)
{
  HandleGetData(hdl);

  initFn(hdl);
  logic.beginFn.push(beginFn);
  logic.updateFn.push(updateFn);
  logic.endFn.push(endFn);
  logic.entityHdl.push(hdl);
}

void LogicAdd(Handle hdl, cstr scriptName)
{
  auto &scriptBindings = GetScriptBindings();
  auto it = scriptBindings.find(scriptName);
  if (it == scriptBindings.end())
  {
    LogError("Can't find script");
    assert(false);
  }

  auto &fns = it->second;

  auto initFn = std::get<0>(fns);
  auto beginFn = std::get<1>(fns);
  auto updateFn = std::get<2>(fns);
  auto endFn = std::get<3>(fns);

  LogicAdd(hdl, initFn, beginFn, updateFn, endFn);
}

void LogicBegin()
{
  for (auto beginFn : logic.beginFn)
  {
    beginFn();
  }
}

void LogicRemove(Handle hdl)
{
  auto id = logic.entityHdl.find(hdl);
  logic.endFn[id]();
}

void LogicUpdate()
{
  for (auto updateFn : logic.updateFn)
  {
    updateFn();
  }
}

void LogicClear()
{
  for (auto &endFn : logic.endFn)
  {
    endFn();
  }

  logic.beginFn.clear();
  logic.updateFn.clear();
  logic.endFn.clear();
  logic.entityHdl.clear();
}