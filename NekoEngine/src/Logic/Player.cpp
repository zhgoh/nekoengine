#include "pch.h"
#include "Player.h"

INTERNAL Handle eHdl;

void Player::Init(Handle hdl)
{
  eHdl = hdl;
}

void Player::Begin()
{
}

void Player::Update()
{
  static const auto speed = 10.0f;

  Vec3 movement;
  if (Input::KeyDown(Key::W))
  {
    //Log("Update");
    movement.Set(0.0f, speed, 0.0f);
  }
  else if (Input::KeyDown(Key::S))
  {
    //Log("Update");
    movement.Set(0.0f, -speed, 0.0f);
  }
  
  if (Input::KeyDown(Key::A))
  {
    //Log("Update");
    movement.Set(-speed, 0.0f, 0.0f);
  }
  else if (Input::KeyDown(Key::D))
  {
    //Log("Update");
    movement.Set(speed, 0.0f, 0.0f);
  }

  //TransformGet(eHdl).translation.Add(movement);
  //WorldApplyForce(eHdl, movement);
}

void Player::End()
{
}