#pragma once

namespace LevelManager
{
  void Init(Handle hdl);
  void Begin();
  void Update();
  void End();
}