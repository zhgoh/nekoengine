#include "pch.h"
#include "LevelManager.h"
#include "Core/Scene.h"

static Handle eHdl;

void LevelManager::Init(Handle hdl)
{
  eHdl = hdl;
}

void LevelManager::Begin()
{
}

void LevelManager::Update()
{
  if (Input::KeyPressed(Key::R))
  {
    // Restart
    SceneRestart();
  }

  if (Input::KeyPressed(Key::N))
  {
    // New
    SceneNew();
  }
}

void LevelManager::End()
{

}