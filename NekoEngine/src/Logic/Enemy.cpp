#include "pch.h"
#include "Enemy.h"

static Handle eHdl;

void Enemy::Init(Handle hdl)
{
  eHdl = hdl;
}

void Enemy::Begin()
{
}

void Enemy::Update()
{
  static const auto speed = 0.3f;
  
  TransformGet(eHdl).translation.Add(0.0f, speed, 0.0f);
}

void Enemy::End()
{
  
}