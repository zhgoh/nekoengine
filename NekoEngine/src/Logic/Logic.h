#pragma once

struct LogicInfo;
using VoidFn = void(*)();
using InitFn = void(*)(Handle);

void LogicAdd(Handle hdl, InitFn initFn, VoidFn beginFn, VoidFn updateFn, VoidFn endFn);
void LogicAdd(Handle hdl, cstr scriptName);
void LogicBegin();
void LogicRemove(Handle hdl);
void LogicUpdate();
void LogicClear();