#pragma once

namespace Enemy
{
  void Init(Handle hdl);
  void Begin();
  void Update();
  void End();
}