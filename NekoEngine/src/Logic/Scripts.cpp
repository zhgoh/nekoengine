#include "pch.h"
#include "Scripts.h"
#include "Player.h"
#include "Enemy.h"

#define DEFINE(NAME)                                            \
{ #NAME, { NAME::Init, NAME::Begin, NAME::Update, NAME::End } }

std::unordered_map<std::string, BundleFn> scriptBindings{
  DEFINE(Player),
  DEFINE(Enemy),
};

const std::unordered_map<std::string, BundleFn> &GetScriptBindings()
{
  return scriptBindings;
}
