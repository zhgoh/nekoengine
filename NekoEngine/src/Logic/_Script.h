#pragma once

namespace _Script
{
  void Init(Handle hdl);
  void Begin();
  void Update();
  void End();
}