#pragma once
#include <unordered_map>
#include "Logic.h"

using BundleFn = std::tuple<InitFn, VoidFn, VoidFn, VoidFn>;
const std::unordered_map<std::string, BundleFn> &GetScriptBindings();