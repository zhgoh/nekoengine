#include "pch.h"

static const auto gravity = b2Vec2(0.0f, -10.0f);
static b2World world(gravity);

struct Physics
{
  //Array<Vec3, MAX_ENTITY> translation;
  Array<Handle, MAX_ENTITY> entityHdl;
};

INTERNAL u32     numObjects = 0;
INTERNAL Physics physics;

void WorldInit()
{
  //world.SetDestructionListener(&m_destructionListener);
  //world.SetContactListener(this);
  //world.SetDebugDraw(&g_debugDraw);

  b2BodyDef bodyDef;
  world.CreateBody(&bodyDef);
}

void WorldAddObject(Handle hdl)
{
  HandleGetData(hdl);
  physics.entityHdl.push(hdl);

  ++numObjects;
}

void WorldAddStaticObject(Handle hdl)
{
  WorldAddObject(hdl);

  //b2BodyDef bodyDef;
  //auto m_groundBody = world.CreateBody(&bodyDef);
}

void WorldAddDynamicObject(Handle hdl)
{
  WorldAddObject(hdl);

}

void WorldApplyForce(Handle hdl, const Vec3 &force)
{
}

void WorldSimulate(f32 dt)
{
  world.Step(dt, 1, 1);
}

void WorldRemoveObject(Handle hdl)
{
  
}

// Box2D cpp
#include "Lib/box2d/Box2D/Box2D/Collision/Shapes/b2ChainShape.cpp"
#include "Lib/box2d/Box2D/Box2D/Collision/Shapes/b2CircleShape.cpp"
#include "Lib/box2d/Box2D/Box2D/Collision/Shapes/b2EdgeShape.cpp"
#include "Lib/box2d/Box2D/Box2D/Collision/Shapes/b2PolygonShape.cpp"

#include "Lib/box2d/Box2D/Box2D/Collision/b2BroadPhase.cpp"
#include "Lib/box2d/Box2D/Box2D/Collision/b2CollideCircle.cpp"
#include "Lib/box2d/Box2D/Box2D/Collision/b2CollideEdge.cpp"
#include "Lib/box2d/Box2D/Box2D/Collision/b2CollidePolygon.cpp"
#include "Lib/box2d/Box2D/Box2D/Collision/b2Collision.cpp"
#include "Lib/box2d/Box2D/Box2D/Collision/b2Distance.cpp"
#include "Lib/box2d/Box2D/Box2D/Collision/b2DynamicTree.cpp"
#include "Lib/box2d/Box2D/Box2D/Collision/b2TimeOfImpact.cpp"

#include "Lib/box2d/Box2D/Box2D/Common/b2BlockAllocator.cpp"
#include "Lib/box2d/Box2D/Box2D/Common/b2Draw.cpp"
#include "Lib/box2d/Box2D/Box2D/Common/b2Math.cpp"
#include "Lib/box2d/Box2D/Box2D/Common/b2Settings.cpp"
#include "Lib/box2d/Box2D/Box2D/Common/b2StackAllocator.cpp"
#include "Lib/box2d/Box2D/Box2D/Common/b2Timer.cpp"

#include "Lib/box2d/Box2D/Box2D/Dynamics/Contacts/b2ChainAndCircleContact.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Contacts/b2ChainAndPolygonContact.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Contacts/b2CircleContact.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Contacts/b2Contact.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Contacts/b2ContactSolver.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Contacts/b2EdgeAndCircleContact.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Contacts/b2EdgeAndPolygonContact.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Contacts/b2PolygonAndCircleContact.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Contacts/b2PolygonContact.cpp"

#include "Lib/box2d/Box2D/Box2D/Dynamics/Joints/b2DistanceJoint.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Joints/b2FrictionJoint.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Joints/b2GearJoint.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Joints/b2Joint.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Joints/b2MotorJoint.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Joints/b2MouseJoint.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Joints/b2PrismaticJoint.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Joints/b2PulleyJoint.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Joints/b2RevoluteJoint.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Joints/b2RopeJoint.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Joints/b2WeldJoint.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/Joints/b2WheelJoint.cpp"

#include "Lib/box2d/Box2D/Box2D/Dynamics/b2Body.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/b2ContactManager.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/b2Fixture.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/b2Island.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/b2World.cpp"
#include "Lib/box2d/Box2D/Box2D/Dynamics/b2WorldCallbacks.cpp"