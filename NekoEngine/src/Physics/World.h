#pragma once

void WorldAddStaticObject(Handle hdl);
void WorldAddDynamicObject(Handle hdl);

void WorldApplyForce(Handle hdl, const Vec3 &force);
void WorldSimulate(f32 dt);
void WorldRemoveObject(Handle hdl);