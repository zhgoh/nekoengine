#pragma once

#include <json.hpp>
#include <Lib/gl3w/include/GL/gl3w.h>
#include <Lib/soil2/SOIL2.h>
#include <Lib/imgui/imgui.h>
#include <Lib/box2d/Box2D/Box2D/Box2D.h>
//#include <Lib/>

#include <Windows.h>
#include <stdint.h>

#include <climits>
#include <cassert>
#include <cstdlib>

#include <array>
#include <string>
#include <stack>
#include <unordered_map>
#include <map>
#include <vector>
#include <fstream>
#include <functional>
#include <thread>
#include <mutex>
#include <queue>
#include <future>

#include "Core/Defines.h"
#include "Utils/Logger.h"
#include "Utils/Array.h"
#include "Utils/Input.h"
#include "Utils/ID.h"
#include "Utils/String.h"
#include "Utils/Window.h"

#include "Math/Math.h"
#include "Math/Vec2.h"
#include "Math/Vec3.h"
#include "Math/Vec4.h"
#include "Math/Mat3.h"
#include "Math/Mat4.h"
#include "Math/Quat.h"

#include "Core/Transform.h"
#include "Core/Handle.h"

#include "Graphics/ImUtils.h"
#include "Graphics/Mesh.h"
#include "Graphics/Shader.h"
#include "Graphics/Texture.h"
#include "Graphics/DisplayList.h"
#include "Graphics/Renderer.h"
#include "Graphics/SpriteBatch.h"


#include "Physics/World.h"

#include "Core/Engine.h"
#include "Core/Entity.h"

#include "Logic/Logic.h"