#pragma once
#include "Math/Mat4.h"
#include <assert.h>
#include "Utils/ThreadPool.h"
#define TEST 1

inline bool operator==(const Vec4 &lhs, const Vec4 &rhs)
{
  return
    fabs(lhs.GetX() - rhs.GetX()) < FLT_EPSILON &&
    fabs(lhs.GetY() - rhs.GetY()) < FLT_EPSILON &&
    fabs(lhs.GetZ() - rhs.GetZ()) < FLT_EPSILON &&
    fabs(lhs.GetW() - rhs.GetW()) < FLT_EPSILON;
}

inline void Test()
{
#if TEST
  {
    Vec3 i(1.0f, 0.0f, 0.0f);
    Vec3 j(0.0f, 1.0f, 0.0f);
    auto val = Dot(i, j);
    assert(val == 0);

    auto k = Cross(i, j);
    assert(k.GetX() == 0.0f);
    assert(k.GetY() == 0.0f);
    assert(k.GetZ() == 1.0f);

    k = Cross(j, i);
    assert(k.GetX() == 0.0f);
    assert(k.GetY() == 0.0f);
    assert(k.GetZ() == -1.0f);

    assert(i.SqLength() == 1.0f);
    assert(fabs(1.0f - i.Normalize().Length()) < 0.0001f);

    i = Vec3(10.0f, 20.0f, 5.0f);
    assert(fabs(1.0f - i.Normalize().Length()) < 0.0001f);

    i = Vec3(54.0f, 84.0f, 12.0f);
    j = Vec3(4.0f, 75.0f, 6.0f);

    k = Cross(i, j);
    assert(k.GetX() == -396.0f);
    assert(k.GetY() == -276.0f);
    assert(k.GetZ() == 3714.0f);
  }

  {
    auto m = Mat4();
    assert(m.GetCol(0) == Vec4(1.0f, 0.0f, 0.0f, 0.0f));
    assert(m.GetCol(1) == Vec4(0.0f, 1.0f, 0.0f, 0.0f));
    assert(m.GetCol(2) == Vec4(0.0f, 0.0f, 1.0f, 0.0f));
    assert(m.GetCol(3) == Vec4(0.0f, 0.0f, 0.0f, 1.0f));

    assert(m.GetRow(0) == Vec4(1.0f, 0.0f, 0.0f, 0.0f));
    assert(m.GetRow(1) == Vec4(0.0f, 1.0f, 0.0f, 0.0f));
    assert(m.GetRow(2) == Vec4(0.0f, 0.0f, 1.0f, 0.0f));
    assert(m.GetRow(3) == Vec4(0.0f, 0.0f, 0.0f, 1.0f));

    const auto VAL = 20.0f;
    m = Mat4::Scale(VAL);
    assert(m.GetCol(0) == Vec4(VAL, 0.0f, 0.0f, 0.0f));
    assert(m.GetCol(1) == Vec4(0.0f, VAL, 0.0f, 0.0f));
    assert(m.GetCol(2) == Vec4(0.0f, 0.0f, VAL, 0.0f));
    assert(m.GetCol(3) == Vec4(0.0f, 0.0f, 0.0f, 1.0f));

    assert(m.GetRow(0) == Vec4(VAL, 0.0f, 0.0f, 0.0f));
    assert(m.GetRow(1) == Vec4(0.0f, VAL, 0.0f, 0.0f));
    assert(m.GetRow(2) == Vec4(0.0f, 0.0f, VAL, 0.0f));
    assert(m.GetRow(3) == Vec4(0.0f, 0.0f, 0.0f, 1.0f));

    m = Mat4::Translate(Vec3(VAL, VAL, VAL));
    assert(m.GetCol(0) == Vec4(1.0f, 0.0f, 0.0f, 0.0f));
    assert(m.GetCol(1) == Vec4(0.0f, 1.0f, 0.0f, 0.0f));
    assert(m.GetCol(2) == Vec4(0.0f, 0.0f, 1.0f, 0.0f));
    assert(m.GetCol(3) == Vec4(VAL, VAL, VAL, 1.0f));

    assert(m.GetRow(0) == Vec4(1.0f, 0.0f, 0.0f, VAL));
    assert(m.GetRow(1) == Vec4(0.0f, 1.0f, 0.0f, VAL));
    assert(m.GetRow(2) == Vec4(0.0f, 0.0f, 1.0f, VAL));
    assert(m.GetRow(3) == Vec4(0.0f, 0.0f, 0.0f, 1.0f));
  }

  {
    Mat4 m1
    {
      1.0f, 2.0f, 3.0f, 4.0f,
      1.0f, 2.0f, 3.0f, 4.0f,
      1.0f, 2.0f, 3.0f, 4.0f,
      1.0f, 2.0f, 3.0f, 4.0f
    };

    Mat4 m2
    {
      2.0f, 3.0f, 4.0f, 1.0f,
      2.0f, 3.0f, 4.0f, 1.0f,
      2.0f, 3.0f, 4.0f, 1.0f,
      2.0f, 3.0f, 4.0f, 1.0f
    };

    Vec4 v1(3.0f, 5.0f, 4.0f, 20.0f);

    auto m3 = m1 * m2;
    assert(m3.GetRow(0) == Vec4(20.0f, 30.0f, 40.0f, 10.0f));
    assert(m3.GetRow(1) == Vec4(20.0f, 30.0f, 40.0f, 10.0f));
    assert(m3.GetRow(2) == Vec4(20.0f, 30.0f, 40.0f, 10.0f));
    assert(m3.GetRow(3) == Vec4(20.0f, 30.0f, 40.0f, 10.0f));

    m1 =
    {
      8.0f, 4.0f, 77.0f, 4.0f,
      7.0f, 8.0f, 89.0f, 4.0f,
      4.0f, 3.0f, 37.0f, 47.0f,
      7.0f, 23.0f, 3.0f, 1.0f
    };

    m2 =
    {
      7.0f, 23.0f, 52.0f, 79.0f,
      94.0f, 3.0f, 65.0f, 47.0f,
      94.0f, 25.0f, 14.0f, 10.0f,
      64.0f, 20.0f, 31.0f, 78.0f
    };

    m3 = m1 * m2;

    assert(m3.GetRow(0) == Vec4(7926.0f, 2201.0f, 1878.0f, 1902.0f));
    assert(m3.GetRow(1) == Vec4(9423.0f, 2490.0f, 2254.0f, 2131.0f));
    assert(m3.GetRow(2) == Vec4(6796.0f, 1966.0f, 2378.0f, 4493.0f));
    assert(m3.GetRow(3) == Vec4(2557.0f, 325.0f, 1932.0f, 1742.0f));

    //m3 = m1.Inverse();
    //assert(m3.GetRow(0) == Vec4(-662889244.0f / 166270029647.0f, -39611.0f / 128261.0f, -94.0f / 18323.0f, 5886.0f / 128261.0f));
    //assert(m3.GetRow(1) == Vec4(-3887.0f / 36646.0f, 3315.0f / 36646.0f, 25.0f / 36646.0f, 1113.0f / 36646.0f));
    //assert(m3.GetRow(2) == Vec4(-333.0f / 18323.0f, 3520.0f / 128261.0f, -12.0f / 18323.0f, -808.0f / 128261.0f));
    //assert(m3.GetRow(3) == Vec4(-343.0f / 36646.0f, -281.0f / 256522.0f, 813.0f / 36646.0f, -227.0f / 256522.0f));


    v1 = m1 * v1;
    assert(v1.GetX() == 432.0f);
    assert(v1.GetY() == 497.0f);
    assert(v1.GetZ() == 1115.0f);
    assert(v1.GetW() == 168.0f);
  }

  {
    {
      Array<int, 10> arr;
      arr.push(10);
      arr.push(30);
      arr.push(20);
      arr.push(40);

      assert(arr.size() == 4);

      arr.pop();
      assert(arr.size() == 3);
      assert(arr.last() == 20);

      arr.push(50);
      arr.remove(10);
      assert(arr.size() == 3);
      assert(arr.last() == 20);
      assert(arr[0] == 50);
      // 50 30 20
      arr.shift(1);
      // 50 20
      assert(arr[0] == 50);
      assert(arr[1] == 20);
      assert(arr.size() == 2);
      arr.shift(0);
      // 20
      assert(arr[0] == 20);
      assert(arr.size() == 1);
      arr.shift(0);
      assert(arr.size() == 0);

      arr.push(10);
      arr.push(70);
      arr.push(100);
      arr.push(5);
      arr.push(24);
      // 10 70 100 5 24
      arr.shift(1, 3);
      // 10 24
      assert(arr[0] == 10);
      assert(arr.size() == 2);

      auto arr2 = arr;
      assert(arr.size() == 2);
      assert(arr2.size() == 2);
      assert(arr[0] == 10);
      assert(arr2[0] == 10);
      assert(arr2[1] == 24);

      auto arr3 = std::move(arr2);
      assert(arr2.size() == 0);
      assert(arr3.size() == 2);
      assert(arr3[0] == 10);
      assert(arr3[1] == 24);
    }

    //for (auto &elem : arr)
    {
      //Log(elem);
    }

    {
      //TEST: 1 invalid handle warnings
      int a;
      auto handle = HandleCreate(Handle::TYPE::ENTITY, &a);
      auto ptr = HandleGetData(handle);
      assert(handle.index == 0);
      assert(handle.counter == 1);
      assert(ptr == &a);
      HandleDestroy(handle);
      ptr = HandleGetData(handle);
      assert(!ptr);

      handle = HandleCreate(Handle::TYPE::ENTITY, &a);
      assert(handle.index == 0);
      assert(handle.counter == 2);
      ptr = HandleGetData(handle);
      assert(ptr == &a);
    }

    {
      char buf[10];
      ne_strcpy(buf, "aaaa");
      assert(strcmp(buf, "aaaa") == 0);

      ne_strcpy(buf, "1234", "bb", "as", "1");
      assert(strcmp(buf, "1234bbas1") == 0);

      // Should fail
      try
      {
        //ne_strcpy(buf, "aaaaaaaaaaaa");
      }
      catch(...)
      {
        Log("Correct behavior test string 1");
      }

      try
      {
        //ne_strcpy(buf, "12345", "67891");
      }
      catch(...)
      {
        Log("Correct behavior test string 2");
      }
    }

    {
      ThreadPool pool(4);
      auto result = pool.Enqueue([](int answer) { return answer; }, 42);

     Log(result.get());
    }
  }
#endif
}