#include "pch.h"

struct Entity
{
  u64 id;
};

INTERNAL Array<Entity, MAX_ENTITY> entities;
INTERNAL Array<char[MAX_STR_LEN], MAX_ENTITY> entityNames;

Handle EntityAdd(cstr name)
{
  return EntityAdd(name, IDGen());
}

Handle EntityAdd(cstr name, u32 id)
{
  entities.increment();
  auto &e = entities.last();
  e.id = id;
  IDUse(id);

  entityNames.increment();
  auto &ename = entityNames.last();

  strcpy_s(ename, name);
  return HandleCreate(Handle::TYPE::ENTITY, &e);
}

void EntityRemove(Handle hdl)
{
  Entity entity;
  auto get = HandleGetData<Entity>(hdl, entity);
  assert(get);

  IDReuse(hdl.index);
  entities.remove(entity);

  // Get all the managers to remove this entity
  TransformRemove(hdl);
  DisplayListRemove(hdl);
  LogicRemove(hdl);
}

bool operator==(const Entity &lhs, const Entity &rhs)
{
  return lhs.id == rhs.id;
}