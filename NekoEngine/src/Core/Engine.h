#pragma once

void EngineStartUp();
void EngineShutDown();

void EngineStop();
bool EngineIsRunning();

void EngineUpdate(f32 dt);