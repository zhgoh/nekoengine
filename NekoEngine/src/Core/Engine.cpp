#include "pch.h"
#include "Assets.h"
#include "Scene.h"

INTERNAL bool isRunning = false;

void EngineStartUp()
{
  isRunning = true;

  //TODO: Take a look where IDInit should be called
  SceneNew();

  // Load assets
  // AssetsLoad();

  //SceneLoad("Test");
  {
    /*auto &mesh = MeshGet("cube");
    auto &shader = ShaderGet("default");
    auto &texture = TextureGet("wall");

    auto hdl = EntityAdd("a");
    TransformAdd(hdl, IDGen());
    DisplayListAdd(hdl, mesh, shader, texture);*/
  }

  //2D sprites
  {
    SpriteBatchInit("test", 512.0f, 512.0f);
    SpriteBatchSet("Player", 0.0f, 384.0f, 128.0f, 128.0f);
    SpriteBatchSet("Wall", 128.0f, 0.0f, 128.0f, 128.0f);

    auto hdl1 = EntityAdd("1");
    TransformAdd(hdl1, IDGen(), Vec3(), Vec3(), Vec3(100.0f, 100.0f, 0.0f));
    SpriteAdd(hdl1, "Wall");
    //WorldAddStaticObject(hdl1);

    auto hdl2 = EntityAdd("2");
    TransformAdd(hdl2, IDGen(), Vec3(2.0f, 4.0f, 0.0f), Vec3(), Vec3(50.0f, 50.0f, 0.0f));
    SpriteAdd(hdl2, "Player");
    WorldAddStaticObject(hdl2);
    SpriteRemove(hdl2);
    //WorldRemoveObject(hdl2);

    SpriteAdd(hdl2, "Player");
    LogicAdd(hdl2, "Player");
    //WorldAddDynamicObject(hdl2);
  }
}

void EngineShutDown()
{
  MeshUnload();
  SpriteBatchUnload();
  RendererShutDown();
}

void EngineStop()
{
  isRunning = false;
}

bool EngineIsRunning()
{
  return isRunning;
}

void EngineUpdate(f32 dt)
{
  Input::Update();
  WorldSimulate(dt);
  TransformUpdate();
  LogicUpdate();
}
