#pragma once

struct Handle;
struct Transform
{
  Transform(Vec3 &scale, Vec3 &rotation, Vec3 &translation) :
    scale(scale),
    rotation(rotation),
    translation(translation)
  {
  }

  Vec3 &scale;
  Vec3 &rotation;
  Vec3 &translation;
};

using TransformMatrix = Array<Mat4, MAX_ENTITY>;

void                  TransformInit();
void                  TransformUpdate();
const TransformMatrix &TransformGetMatrix();
int                   TransformGetIndex(Handle hdl);
Transform             TransformGet(Handle hdl);
void                  TransformRemove(Handle hdl);
void                  TransformAdd(Handle hdl, u32 id, Vec3 trans = Vec3(), Vec3 rot = Vec3(), Vec3 scale = Vec3(1.0f, 1.0f, 1.0f), u32 pid = -1);
void                  TransformClear();