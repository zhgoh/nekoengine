#include "pch.h"
#include "Handle.h"
#include <cassert>

Handle::Handle() :
  index(0), counter(0), type(EMPTY)
{
}

Handle::Handle(u32 index, u32 counter, TYPE type) :
  index(index), counter(counter), type(type)
{
}

struct HandleEntry
{
  HandleEntry() :
    isUsing(false), count(0), ptr(nullptr)
  {
  }

  bool isUsing;
  u32 count;
  void *ptr;
};

INTERNAL std::array<std::array<HandleEntry, MAX_ENTITY>, 1> handleEntries;

INTERNAL
HandleEntry &HandleGetEntry(u32 type, u32 index)
{
  auto &entry = handleEntries[type][index];
#if _DEBUG
  if (!entry.isUsing)
  {
    LogWarning("Using invalid handle");
  }
#endif
  return entry;
}

INTERNAL
HandleEntry &HandleGetEntry(Handle handle)
{
  return HandleGetEntry(handle.type, handle.index);
}

INTERNAL
std::pair<u32, u32> GetFreeIndex(Handle::TYPE type)
{
  auto &handles = handleEntries[type];
  for (u32 i = 0; i < handles.size(); ++i)
  {
    auto &handle = handles[i];
    if (!handle.isUsing)
    {
      ++handle.count;
      return std::make_pair(i, handle.count);
    }
  }

  // No more handles
  throw("No more handles");
}

Handle HandleGet(Handle::TYPE type, u32 index)
{
  auto &entry = HandleGetEntry(type, index);
  return Handle(index, entry.count, type);
}

Handle HandleCreate(Handle::TYPE type, void *data)
{
  auto free = GetFreeIndex(type);
  auto &entry = handleEntries[type][free.first];
  entry.ptr = data;
  entry.isUsing = true;
  return Handle(free.first, free.second, type);
}

void HandleDestroy(Handle handle)
{
  auto &handleEntry = HandleGetEntry(handle);
  handleEntry.isUsing = false;
  handleEntry.ptr = nullptr;
}

void *HandleGetData(Handle handle)
{
  return HandleGetEntry(handle).ptr;
}

bool HandleGetData(Handle handle, void *&out)
{
  assert(!out);
  out = HandleGetData(handle);
  return out != nullptr;
}

bool operator==(Handle lhs, Handle rhs)
{
  return lhs.index == rhs.index &&
         lhs.counter == rhs.counter &&
         lhs.type == rhs.type;
}
