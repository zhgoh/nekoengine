#include "pch.h"
#include "Logic/Player.h"
#include "Logic/Enemy.h"
#include "Logic/LevelManager.h"

#define LoadScript(NAMESPACE, val)                      \
    for (auto &elem : json[val])                        \
    {                                                   \
      auto eid = elem["eid"].get<u32>();                \
      auto hdl = HandleGet(Handle::TYPE::ENTITY, eid);  \
      RegisterLogicNS(hdl, NAMESPACE);                  \
    }

INTERNAL char lastScene[MAX_STR_LEN];

void SceneNew()
{
  IDInit();
  TransformClear();
  DisplayListClear();
  LogicClear();

  TransformInit();
}

void SceneLoad(cstr sceneName)
{
  SceneNew();

  char currentScene[256];
  ne_strcpy(lastScene, sceneName);
  ne_strcpy(currentScene, SCENE, sceneName, SCENE_EXT);

  std::ifstream ifs(currentScene);
  if (ifs.good())
  {
    // read a JSON file
    nlohmann::json json;
    ifs >> json;

    for (auto &elem : json["entity"])
    {
      auto name = move(elem["name"].get<std::string>());
      auto id = elem["id"].get<u32>();

      EntityAdd(name.c_str(), id);
    }

    for (auto &elem : json["transform"])
    {
      auto eid = elem["eid"].get<u32>();
      auto id = elem["id"].get<u32>();
      auto parent = elem["pid"].get<u32>();

      auto &t = elem["translation"];
      auto &r = elem["rotation"];
      auto &s = elem["scale"];

      Vec3 trans(t["x"].get<f32>(), t["y"].get<f32>(), t["z"].get<f32>());
      Vec3 rot(r["x"].get<f32>(), r["y"].get<f32>(), r["z"].get<f32>());
      Vec3 scale(s["x"].get<f32>(), s["y"].get<f32>(), s["z"].get<f32>());

      auto hdl = HandleGet(Handle::TYPE::ENTITY, eid);
      TransformAdd(hdl, id, trans, rot, scale, parent);
    }

    for (auto &elem : json["graphics"])
    {
      auto eid = elem["eid"].get<u32>();
      //auto id = elem["id"].get<u32>();
      auto meshName     = move(elem["mesh"].get<std::string>());
      auto textureName  = move(elem["texture"].get<std::string>());
      auto shaderName   = move(elem["shader"].get<std::string>());

      auto &mesh    = MeshGet(meshName.c_str());
      auto &texture = TextureGet(textureName.c_str());
      auto &shader  = ShaderGet(shaderName.c_str());

      auto hdl = HandleGet(Handle::TYPE::ENTITY, eid);
      DisplayListAdd(hdl, mesh, shader, texture);
    }

    LoadScript(Player, "player");
    LoadScript(Enemy, "enemy");
    LoadScript(LevelManager, "levelmanager");

    LogicBegin();
    return;
  }

  LogWarning("Cannot find scene file ", sceneName);
}

void SceneSave(cstr name)
{
  std::ofstream ofs(name);
  if (ofs.good())
  {
    nlohmann::json json;
    // write prettified JSON to another file
    ofs << std::setw(4) << json << std::endl;
  }

}

void SceneRestart()
{
  SceneNew();
  SceneLoad(lastScene);
}