#pragma once

struct Handle
{
  enum TYPE : u32
  {
    ENTITY,
    EMPTY
  };

  Handle();
  Handle(u32 index, u32 counter, TYPE type);

  u32 index : 12;
  u32 counter : 15;
  TYPE type : 5;
};

Handle HandleGet(Handle::TYPE type, u32 index);
Handle HandleCreate(Handle::TYPE type, void *data);
void   HandleDestroy(Handle handle);
void  *HandleGetData(Handle handle);
bool   HandleGetData(Handle handle, void *&out);
template<typename T>
bool HandleGetData(Handle handle, T &out)
{
  void *data = nullptr;
  auto ret = HandleGetData(handle, data);
  out = *static_cast<T *>(data);

  return ret;
}
bool operator==(Handle lhs, Handle rhs);