#include "pch.h"

void AssetsLoad()
{
  std::ifstream ifs(ASSETS_LIST);
  if (ifs.good())
  {
    // Load from Assets.json for the list of assets
    nlohmann::json json;
    ifs >> json;

    for (auto &elem : json["meshes"])
    {
      auto name = move(elem.get<std::string>());
      MeshLoad(name.c_str());
    }

    for (auto &elem : json["textures"])
    {
      auto name = move(elem.get<std::string>());
      TextureLoad(name.c_str());
    }

    for (auto &elem : json["shaders"])
    {
      auto name = move(elem["name"].get<std::string>());
      auto vsFile = move(elem["vs"].get<std::string>());
      auto fsFile = move(elem["fs"].get<std::string>());

      auto &shader = ShaderLoad(name.c_str(), vsFile.c_str(), fsFile.c_str());
      for (auto &loc : elem["location"])
      {
        ShaderGetUniform(shader, loc.get<std::string>().c_str());
      }
    }
    return;
  }
  
  assert(false);
  LogError("Missing assets.json file in assets");
}

