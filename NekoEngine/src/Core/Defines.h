#pragma once

using i8                    = int8_t;
using i16                   = int16_t;
using i32                   = int32_t;
using i64                   = int64_t;

using u8                    = uint8_t;
using u16                   = uint16_t;
using u32                   = uint32_t;
using u64                   = uint64_t;

using f32                   = float;
using f64                   = double;

using cstr                  = const char *;

const auto MAX_ENTITY       = 100;
const auto MAX_COMPONENT    = MAX_ENTITY;
const auto MAX_LOGIC        = MAX_ENTITY;
const auto MAX_MESH         = MAX_ENTITY;
const auto MAX_TEXTURE      = MAX_ENTITY;
const auto MAX_SHADER       = MAX_ENTITY;
const auto MAX_SPRITE       = MAX_ENTITY;
const auto MAX_PHYSICS      = MAX_ENTITY;
const auto MAX_STR_LEN      = 256;

#define ASSETS              "./Assets/"
#define ASSETS_LIST         ASSETS"Assets.json"
#define SCENE               ASSETS"Scenes/"
#define SCENE_EXT           ".json"
#define MESH                ASSETS"Meshes/"
#define MESH_EXT            ".obj"
#define TEXTURE             ASSETS"Textures/"
#define TEXTURE_EXT         ".png"
#define SHADER              ASSETS"Shaders/"
#define VS_EXT              ".vert"
#define FS_EXT              ".frag"

#define INTERNAL static

#define RegisterLogicSTR(HANDLE, SCRIPTNAME)      \
{                                                 \
  LogicAdd(HANDLE, SCRIPTNAME);                   \
}

#define RegisterLogicNS(HANDLE, NAMESPACE)    \
{                                             \
  using namespace NAMESPACE;                  \
  LogicAdd(HANDLE, Init, Begin, Update, End); \
}