#pragma once

struct Entity;
Handle EntityAdd(cstr name);
Handle EntityAdd(cstr name, u32 id);
void   EntityRemove(Handle hdl);
bool   operator==(const Entity &lhs, const Entity &rhs);