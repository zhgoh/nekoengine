#include "pch.h"
#include "Handle.h"

struct Transforms
{
  Array<u32,    MAX_ENTITY> id;
  Array<u32,    MAX_ENTITY> pid;
  Array<Vec3,   MAX_ENTITY> translation;
  Array<Vec3,   MAX_ENTITY> rotation;
  Array<Vec3,   MAX_ENTITY> scale;
  Array<Mat4,   MAX_ENTITY> mat;
  Array<Handle, MAX_ENTITY> entityHdl;
};

INTERNAL u32        numObjects = 0;
INTERNAL Transforms transforms;

void SpriteBatchRegister(
  const Array<Vec3, MAX_ENTITY> &translation, 
  const Array<Vec3, MAX_ENTITY> &rotation, 
  const Array<Vec3, MAX_ENTITY> &scale);

// NOTE: World register without const
//void WorldRegister(
//  Array<Vec3, MAX_ENTITY> &translation, 
//  Array<Vec3, MAX_ENTITY> &rotation, 
//  Array<Vec3, MAX_ENTITY> &scale);

void TransformInit()
{
  SpriteBatchRegister(transforms.translation, transforms.rotation, transforms.scale);
}

void TransformAdd(Handle hdl, u32 id, Vec3 trans, Vec3 rot, Vec3 scale, u32 pid)
{
  HandleGetData(hdl);
  transforms.entityHdl.push(hdl);

  ++numObjects;

  transforms.id.push(id);
  transforms.pid.push(pid);
  transforms.translation.push(std::move(trans));
  transforms.rotation.push(std::move(rot));
  transforms.scale.push(std::move(scale));
  transforms.mat.push(std::move(Mat4()));
}

void TransformUpdate()
{
  for (u32 i = 0; i < numObjects; ++i)
  {
    const auto &translation = transforms.translation[i];
    const auto &rotation = transforms.rotation[i];
    const auto &scale = transforms.scale[i];

    auto translationMat = Mat4::Translate(translation);
    auto rotationMat =
      Mat4::Rotate(Vec3::XRIGHT, rotation.GetX())
      *
      Mat4::Rotate(Vec3::ZFORWARD, rotation.GetZ())
      *
      Mat4::Rotate(Vec3::YUP, rotation.GetY());
    auto scaleMat = Mat4::Scale(scale);

    transforms.mat[i] = translationMat * rotationMat * scaleMat;
  }
}

const TransformMatrix &TransformGetMatrix()
{
  return transforms.mat;
}

int TransformGetIndex(Handle hdl)
{
  return transforms.entityHdl.find(hdl);
}

Transform TransformGet(Handle hdl)
{
  HandleGetData(hdl);
  auto id = transforms.entityHdl.find(hdl);
  assert(id != -1);

  return Transform{ transforms.scale[id], transforms.rotation[id], transforms.translation[id] };
}

void TransformRemove(Handle hdl)
{
  HandleGetData(hdl);

  auto id = transforms.entityHdl.remove(hdl);
  assert(id != -1);

  --numObjects;
  transforms.id.removeAt(id);
  transforms.pid.removeAt(id);
  transforms.translation.removeAt(id);
  transforms.rotation.removeAt(id);
  transforms.scale.removeAt(id);
  transforms.mat.removeAt(id);
}

void TransformClear()
{
  numObjects = 0;
  transforms.id.clear();
  transforms.pid.clear();
  transforms.translation.clear();
  transforms.rotation.clear();
  transforms.scale.clear();
  transforms.mat.clear();
  transforms.entityHdl.clear();
}