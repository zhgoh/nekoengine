#include "pch.h"
#include "Vec2.h"

Vec2::Vec2(f32 x, f32 y)
  : mX{ x }, mY{ y }
{
}

Vec2::Vec2(f32(&data)[2])
  : mX{ data[0] }, mY{ data[1] }
{
}

Vec2::Vec2(const Vec3 &v) :
 mX(v.GetX()), mY(v.GetY())
{
}

f32 Vec2::GetX() const
{
  return mX;
}

f32 Vec2::GetY() const
{
  return mY;
}

void Vec2::SetX(f32 x)
{
  mX = x;
}

void Vec2::SetY(f32 y)
{
  mY = y;
}

void Vec2::Set(f32 x, f32 y)
{
  mX = x;
  mY = y;
}

f32 Vec2::Length() const
{
  return sqrt(SqLength());
}

f32 Vec2::SqLength() const
{
  return mX * mX + mY * mY;
}

Vec2 Vec2::Normalize() const
{
  const auto invSq = InvSqrt(SqLength());
  return Vec2(mX * invSq, mY * invSq);
}

Vec2 &Vec2::SetNormalize()
{
  const auto invSq = InvSqrt(SqLength());
  mX *= invSq;
  mY *= invSq;

  return *this;
}

std::ostream &operator<<(std::ostream &os, const Vec2 &v)
{
  return os << "{ x: " << v.GetX() << " y: " << v.GetY() << " }";
}

Vec2 operator+(const Vec2 &lhs, const Vec2 &rhs)
{
  return Vec2(lhs.GetX() + rhs.GetX(), lhs.GetY() + rhs.GetY());
}

Vec2 operator-(const Vec2 & lhs, const Vec2 & rhs)
{
  return Vec2(lhs.GetX() - rhs.GetX(), lhs.GetY() - rhs.GetY());
}

Vec2 operator*(f32 scalar, const Vec2 &rhs)
{
  return Vec2(scalar * rhs.GetX(), scalar * rhs.GetY());
}

Vec2 operator*(const Vec2 &lhs, f32 scalar)
{
  return operator*(scalar, lhs);
}

f32 Dot(const Vec2 &lhs, const Vec2 &rhs)
{
  return lhs.GetX() * rhs.GetX() +
         lhs.GetY() * rhs.GetY();
}
