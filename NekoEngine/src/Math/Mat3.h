#pragma once

class Mat3
{
public:
  Mat3();
  Mat3(f32 m00, f32 m01, f32 m02,
       f32 m10, f32 m11, f32 m12,
       f32 m20, f32 m21, f32 m22);

  Mat3 Inverse() const;
  Mat3 Transpose() const;
  Mat3 Scale(f32 scale);
  Mat3 Rotate(f32 angleRad);
  Mat3 Translate(Vec3 translation);

  f32 &operator[](size_t index);
  f32  operator[](size_t index) const;

private:
  f32 mMat[9];

  Vec3 GetRow(int r) const;
  Vec3 GetCol(int c) const;
  f32 Determinant() const;

  friend Mat3 operator*(const Mat3 &lhs, const Mat3 &rhs);
};

Mat3 operator*(const Mat3 &lhs, const Mat3 &rhs);
