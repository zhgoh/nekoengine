#include "pch.h"

f32 InvSqrt(f32 number)
{
  const auto threehalfs = 1.5f;
  const auto x2 = number * 0.5F;
  auto y = number;
  auto i = *reinterpret_cast<long *>(&y); // evil floating point bit level hacking
  i = 0x5f3759df - (i >> 1);              // what the fuck? 
  y = *reinterpret_cast<float *>(&i);
  y *= threehalfs - (x2 * y * y);         // 1st iteration
  y *= threehalfs - (x2 * y * y);         // 2nd iteration, this can be removed
  return y;
}