#include "pch.h"

Mat3::Mat3()
  : mMat{
  1.0f, 0.0f, 0.0f,
  0.0f, 1.0f, 0.0f,
  0.0f, 0.0f, 1.0f
}
{
}

Mat3::Mat3(f32 m00, f32 m01, f32 m02,
           f32 m10, f32 m11, f32 m12,
           f32 m20, f32 m21, f32 m22)
  : mMat{
  m00, m10, m20,
  m01, m11, m21,
  m02, m12, m22
}
{
}

Mat3 Mat3::Inverse() const
{
  Mat3 m;
  if (fabs(Determinant() < FLT_EPSILON))
  {
    return m;
  }


}

Vec3 Mat3::GetRow(int r) const
{
  return Vec3(mMat[r], mMat[r + 3], mMat[r + 6]);
}

Vec3 Mat3::GetCol(int c) const
{
  c *= 3;
  return Vec3(mMat[c], mMat[c + 1], mMat[c + 2]);
}
// 0 3 6      0 1 2
// 1 4 7      3 4 5
// 2 5 8      6 7 8
f32 Mat3::Determinant() const
{
  return (
    mMat[4] * mMat[8] - mMat[7] * mMat[5]
    );
}

Mat3 operator*(const Mat3 &lhs, const Mat3 &rhs)
{
  Vec3 rowOne   = lhs.GetRow(0);
  Vec3 rowTwo   = lhs.GetRow(1);
  Vec3 rowThree = lhs.GetRow(2);

  Vec3 colOne   = rhs.GetCol(0);
  Vec3 colTwo   = rhs.GetCol(1);
  Vec3 colThree = rhs.GetCol(2);

  return Mat3
  {
    colOne * rowOne,   colTwo * rowOne,   colThree * rowOne,
    colOne * rowTwo,   colTwo * rowTwo,   colThree * rowTwo,
    colOne * rowThree, colTwo * rowThree, colThree * rowThree,
  };
}

f32 &Mat3::operator[](size_t index)
{
  return mMat[index];
}

f32 Mat3::operator[](size_t index) const
{
  return const_cast<Mat3 &>(*this)[index];
}

