#pragma once

class Quat
{
public:
  explicit Quat(f32 w = 1.0f, f32 x = 0.0f, f32 y = 0.0f, f32 z = 0.0f);

  f32 GetX() const;
  f32 GetY() const;
  f32 GetZ() const;
  f32 GetW() const;

  void SetX(f32 x);
  void SetY(f32 y);
  void SetZ(f32 z);
  void SetW(f32 w);

  f32 Length() const;
  f32 SqLength() const;

  Quat Normalize() const;
  Quat &SetNormalize();
  void SetIdentity();

  Quat &operator+=(const Quat &rhs);
  Quat &operator-=(const Quat &rhs);

  static Quat Slerp(const Quat &a, const Quat &b, float t);

private:
  f32 mX;
  f32 mY;
  f32 mZ;
  f32 mW;
};

std::ostream &operator<<(std::ostream &os, const Quat &q);

Quat operator+(const Quat &lhs, const Quat &rhs);
Quat operator-(const Quat &lhs, const Quat &rhs);
Quat operator*(f32 scalar,      const Quat &rhs);
Quat operator*(const Quat &lhs, f32 scalar);
Quat operator*(const Quat &lhs, const Quat &rhs);