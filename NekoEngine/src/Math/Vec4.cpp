#include "pch.h"

Vec4::Vec4(f32 x, f32 y, f32 z, f32 w)
  : mX{ x }, mY{ y }, mZ{ z }, mW{ w }
{
}

f32 Vec4::GetX() const
{
  return mX;
}

f32 Vec4::GetY() const
{
  return mY;
}

f32 Vec4::GetZ() const
{
  return mZ;
}

f32 Vec4::GetW() const
{
  return mW;
}

void Vec4::SetX(f32 x)
{
  mX = x;
}

void Vec4::SetY(f32 y)
{
  mY = y;
}

void Vec4::SetZ(f32 z)
{
  mZ = z;
}

void Vec4::SetW(f32 w)
{
  mW = w;
}

void Vec4::Set(f32 x, f32 y, f32 z, f32 w)
{
  mX = x;
  mY = y;
  mZ = z;
  mW = w;
}

f32 Vec4::Length() const
{
  return sqrt(SqLength());
}

f32 Vec4::SqLength() const
{
  return mX * mX + mY * mY + mZ * mZ + mW * mW;
}

Vec4 Vec4::Normalize() const
{
  const auto invSq = InvSqrt(SqLength());
  return Vec4(mX * invSq, mY * invSq, mZ * invSq, mW * invSq);
}

Vec4 &Vec4::SetNormalize()
{
  const auto invSq = InvSqrt(SqLength());
  mX *= invSq;
  mY *= invSq;
  mZ *= invSq;
  mW *= invSq;

  return *this;
}

Vec4 &Vec4::operator+=(const Vec4 &rhs)
{
  mX += rhs.mX;
  mY += rhs.mY;
  mZ += rhs.mZ;
  mW += rhs.mW;

  return *this;
}

Vec4 &Vec4::operator-=(const Vec4 &rhs)
{
  mX -= rhs.mX;
  mY -= rhs.mY;
  mZ -= rhs.mZ;
  mW -= rhs.mW;

  return *this;
}

Vec4 operator+(const Vec4 &lhs, const Vec4 &rhs)
{
  return Vec4(lhs.GetX() + rhs.GetX(),
              lhs.GetY() + rhs.GetY(),
              lhs.GetZ() + rhs.GetZ(),
              lhs.GetW() + rhs.GetW());
}

Vec4 operator-(const Vec4 &lhs, const Vec4 &rhs)
{
  return Vec4(lhs.GetX() - rhs.GetX(),
              lhs.GetY() - rhs.GetY(),
              lhs.GetZ() - rhs.GetZ(),
              lhs.GetW() - rhs.GetW());
}

Vec4 operator*(f32 scalar, const Vec4 &rhs)
{
  return Vec4(scalar * rhs.GetX(),
              scalar * rhs.GetY(),
              scalar * rhs.GetZ(),
              scalar * rhs.GetW());
}

Vec4 operator*(const Vec4 &lhs, f32 scalar)
{
  return operator*(scalar, lhs);
}

Vec4 operator*(const Mat4 &lhs, const Vec4 &rhs)
{
  return Vec4(Dot(lhs.GetRow(0), rhs),
              Dot(lhs.GetRow(1), rhs),
              Dot(lhs.GetRow(2), rhs),
              Dot(lhs.GetRow(3), rhs));
}

f32 Dot(const Vec4 &lhs, const Vec4 &rhs)
{
  return lhs.GetX() * rhs.GetX() +
         lhs.GetY() * rhs.GetY() +
         lhs.GetZ() * rhs.GetZ() +
         lhs.GetW() * rhs.GetW();
}

std::ostream &operator<<(std::ostream &os, const Vec4 &v)
{
  return os << "{ x:" << v.GetX() << " y: " << v.GetY() << " z: " << v.GetZ() << " w: " << v.GetW() << " }";
}
