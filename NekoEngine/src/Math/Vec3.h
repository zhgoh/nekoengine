#pragma once

class Vec3
{
public:
  explicit Vec3(f32 x = 0.0f, f32 y = 0.0f, f32 z = 0.0f);
  explicit Vec3(f32 (&data)[3]);

  f32 GetX() const;
  f32 GetY() const;
  f32 GetZ() const;

  void SetX(f32 x);
  void SetY(f32 y);
  void SetZ(f32 z);

  void Set(f32 x = 0.0f, f32 y = 0.0f, f32 z = 0.0f);

  f32 Length() const;
  f32 SqLength() const;

  Vec3 Normalize() const;
  Vec3 &SetNormalize();

  void Add(f32 x = 0.0f, f32 y = 0.0f, f32 z = 0.0f);
  void Add(const Vec3 &rhs);
  
  Vec3 &operator+=(const Vec3 &rhs);
  Vec3 &operator-=(const Vec3 &rhs);
  Vec3 &operator*=(f32 scalar);

  static const Vec3 XRIGHT;
  static const Vec3 YUP;
  static const Vec3 ZFORWARD;

private:
  f32 mX;
  f32 mY;
  f32 mZ;
};

Vec3  operator+ (const Vec3 &lhs, const Vec3 &rhs);
Vec3  operator- (const Vec3 &lhs, const Vec3 &rhs);
Vec3  operator* (f32 scalar,      const Vec3 &rhs);
Vec3  operator* (const Vec3 &lhs, f32 scalar);
f32   Dot       (const Vec3 &lhs, const Vec3 &rhs);
Vec3  Cross     (const Vec3 &lhs, const Vec3 &rhs);

std::ostream &operator<<(std::ostream &os, const Vec3 &v);