#include "pch.h"

Mat4::Mat4()
  : mMat{
  1.0f, 0.0f, 0.0f, 0.0f,
  0.0f, 1.0f, 0.0f, 0.0f,
  0.0f, 0.0f, 1.0f, 0.0f,
  0.0f, 0.0f, 0.0f, 1.0f
}
{
}

Mat4::Mat4(f32 m00, f32 m01, f32 m02, f32 m03,
           f32 m10, f32 m11, f32 m12, f32 m13,
           f32 m20, f32 m21, f32 m22, f32 m23,
           f32 m30, f32 m31, f32 m32, f32 m33)
  : mMat{
    m00, m10, m20, m30,
    m01, m11, m21, m31,
    m02, m12, m22, m32,
    m03, m13, m23, m33
}
{
}

Mat4::Mat4(const Quat &quat)
{
  const auto tx = 2.0f * quat.GetX();
  const auto ty = 2.0f * quat.GetY();
  const auto tz = 2.0f * quat.GetZ();
  const auto twx = tx * quat.GetW();
  const auto twy = ty * quat.GetW();
  const auto twz = tz * quat.GetW();
  const auto txx = tx * quat.GetX();
  const auto txy = ty * quat.GetX();
  const auto txz = tz * quat.GetX();
  const auto tyy = ty * quat.GetY();
  const auto tyz = tz * quat.GetY();
  const auto tzz = tz * quat.GetZ();

  mMat = { 1.0f - (tyy + tzz), txy - twz,          txz + twy,          0.0f,
           txy + twz,          1.0f - (txx + tzz), tyz - twx,          0.0f,
           txz - twy,          tyz + twx,          1.0f - (txx + tyy), 0.0f,
           0.0f,               0.0f,               0.0f,               1.0f };
}

Mat4 Mat4::Inverse() const
{
  Mat4 mat;
  f32 inv[16];

  inv[0] =
    mMat[5] * mMat[10] * mMat[15] -
    mMat[5] * mMat[11] * mMat[14] -
    mMat[9] * mMat[6] * mMat[15] +
    mMat[9] * mMat[7] * mMat[14] +
    mMat[13] * mMat[6] * mMat[11] -
    mMat[13] * mMat[7] * mMat[10];

  inv[4] =
    -mMat[4] * mMat[10] * mMat[15] +
    mMat[4] * mMat[11] * mMat[14] +
    mMat[8] * mMat[6] * mMat[15] -
    mMat[8] * mMat[7] * mMat[14] -
    mMat[12] * mMat[6] * mMat[11] +
    mMat[12] * mMat[7] * mMat[10];

  inv[8] =
    mMat[4] * mMat[9] * mMat[15] -
    mMat[4] * mMat[11] * mMat[13] -
    mMat[8] * mMat[5] * mMat[15] +
    mMat[8] * mMat[7] * mMat[13] +
    mMat[12] * mMat[5] * mMat[11] -
    mMat[12] * mMat[7] * mMat[9];

  inv[12] =
    -mMat[4] * mMat[9] * mMat[14] +
    mMat[4] * mMat[10] * mMat[13] +
    mMat[8] * mMat[5] * mMat[14] -
    mMat[8] * mMat[6] * mMat[13] -
    mMat[12] * mMat[5] * mMat[10] +
    mMat[12] * mMat[6] * mMat[9];

  inv[1] =
    -mMat[1] * mMat[10] * mMat[15] +
    mMat[1] * mMat[11] * mMat[14] +
    mMat[9] * mMat[2] * mMat[15] -
    mMat[9] * mMat[3] * mMat[14] -
    mMat[13] * mMat[2] * mMat[11] +
    mMat[13] * mMat[3] * mMat[10];

  inv[5] =
    mMat[0] * mMat[10] * mMat[15] -
    mMat[0] * mMat[11] * mMat[14] -
    mMat[8] * mMat[2] * mMat[15] +
    mMat[8] * mMat[3] * mMat[14] +
    mMat[12] * mMat[2] * mMat[11] -
    mMat[12] * mMat[3] * mMat[10];

  inv[9] =
    -mMat[0] * mMat[9] * mMat[15] +
    mMat[0] * mMat[11] * mMat[13] +
    mMat[8] * mMat[1] * mMat[15] -
    mMat[8] * mMat[3] * mMat[13] -
    mMat[12] * mMat[1] * mMat[11] +
    mMat[12] * mMat[3] * mMat[9];

  inv[13] =
    mMat[0] * mMat[9] * mMat[14] -
    mMat[0] * mMat[10] * mMat[13] -
    mMat[8] * mMat[1] * mMat[14] +
    mMat[8] * mMat[2] * mMat[13] +
    mMat[12] * mMat[1] * mMat[10] -
    mMat[12] * mMat[2] * mMat[9];

  inv[2] =
    mMat[1] * mMat[6] * mMat[15] -
    mMat[1] * mMat[7] * mMat[14] -
    mMat[5] * mMat[2] * mMat[15] +
    mMat[5] * mMat[3] * mMat[14] +
    mMat[13] * mMat[2] * mMat[7] -
    mMat[13] * mMat[3] * mMat[6];

  inv[6] =
    -mMat[0] * mMat[6] * mMat[15] +
    mMat[0] * mMat[7] * mMat[14] +
    mMat[4] * mMat[2] * mMat[15] -
    mMat[4] * mMat[3] * mMat[14] -
    mMat[12] * mMat[2] * mMat[7] +
    mMat[12] * mMat[3] * mMat[6];

  inv[10] =
    mMat[0] * mMat[5] * mMat[15] -
    mMat[0] * mMat[7] * mMat[13] -
    mMat[4] * mMat[1] * mMat[15] +
    mMat[4] * mMat[3] * mMat[13] +
    mMat[12] * mMat[1] * mMat[7] -
    mMat[12] * mMat[3] * mMat[5];

  inv[14] =
    -mMat[0] * mMat[5] * mMat[14] +
    mMat[0] * mMat[6] * mMat[13] +
    mMat[4] * mMat[1] * mMat[14] -
    mMat[4] * mMat[2] * mMat[13] -
    mMat[12] * mMat[1] * mMat[6] +
    mMat[12] * mMat[2] * mMat[5];

  inv[3] =
    -mMat[1] * mMat[6] * mMat[11] +
    mMat[1] * mMat[7] * mMat[10] +
    mMat[5] * mMat[2] * mMat[11] -
    mMat[5] * mMat[3] * mMat[10] -
    mMat[9] * mMat[2] * mMat[7] +
    mMat[9] * mMat[3] * mMat[6];

  inv[7] =
    mMat[0] * mMat[6] * mMat[11] -
    mMat[0] * mMat[7] * mMat[10] -
    mMat[4] * mMat[2] * mMat[11] +
    mMat[4] * mMat[3] * mMat[10] +
    mMat[8] * mMat[2] * mMat[7] -
    mMat[8] * mMat[3] * mMat[6];

  inv[11] =
    -mMat[0] * mMat[5] * mMat[11] +
    mMat[0] * mMat[7] * mMat[9] +
    mMat[4] * mMat[1] * mMat[11] -
    mMat[4] * mMat[3] * mMat[9] -
    mMat[8] * mMat[1] * mMat[7] +
    mMat[8] * mMat[3] * mMat[5];

  inv[15] =
    mMat[0] * mMat[5] * mMat[10] -
    mMat[0] * mMat[6] * mMat[9] -
    mMat[4] * mMat[1] * mMat[10] +
    mMat[4] * mMat[2] * mMat[9] +
    mMat[8] * mMat[1] * mMat[6] -
    mMat[8] * mMat[2] * mMat[5];

  auto det = mMat[0] * inv[0] +
    mMat[1] * inv[4] +
    mMat[2] * inv[8] +
    mMat[3] * inv[12];

  if (fabs(det) < FLT_EPSILON)
    return mat;

  det = 1.0f / det;

  for (auto i = 0; i < 16; i++)
    mat[i] = inv[i] * det;

  return mat;
}

Mat4 Mat4::Transpose() const
{
  auto m(*this);
  std::swap(m.mMat[1], m.mMat[4]);
  std::swap(m.mMat[2], m.mMat[8]);
  std::swap(m.mMat[3], m.mMat[12]);

  std::swap(m.mMat[6], m.mMat[9]);
  std::swap(m.mMat[7], m.mMat[13]);
  std::swap(m.mMat[11], m.mMat[14]);
  return m;
}

const f32 *Mat4::Data() const
{
  return mMat.data();
}

Mat4 Mat4::Scale(f32 scale)
{
  return Scale(scale, scale, scale);
}

Mat4 Mat4::Scale(const Vec3 &scale)
{
  return Scale(scale.GetX(), scale.GetY(), scale.GetZ());
}

Mat4 Mat4::Scale(f32 scaleX, f32 scaleY, f32 scaleZ)
{
  return Mat4
  {
    scaleX, 0.0f,   0.0f,   0.0f,
    0.0f,   scaleY, 0.0f,   0.0f,
    0.0f,   0.0f,   scaleZ, 0.0f,
    0.0f,   0.0f,   0.0f,   1.0f
  };
}

Mat4 Mat4::Rotate(const Vec3 &axis, f32 angleRad)
{
  Mat4 m;

  // Normalize u in case u is not normalized
  auto u = axis.Normalize();

  // Axis angle without optimizations
  auto cT = cos(angleRad);
  auto oCT = 1.0f - cT;
  auto sT = sin(angleRad);

  auto uxuy = u.GetX() * u.GetY();
  auto uyuz = u.GetY() * u.GetZ();
  auto uzux = u.GetZ() * u.GetX();

  auto uxSq = u.GetX() * u.GetX();
  auto uySq = u.GetY() * u.GetY();
  auto uzSq = u.GetZ() * u.GetZ();

  m[0] = uxSq + cT * (1.0f - uxSq);  m[1] = uxuy * oCT - u.GetZ() * sT; m[2] = uzux * cT + u.GetY() * sT;  m[3] = 0.0f;
  m[4] = uxuy * oCT + u.GetZ() * sT; m[5] = uySq + cT * (1.0f - uySq);  m[6] = uyuz * oCT - u.GetX() * sT; m[7] = 0.0f;
  m[8] = uzux * oCT - u.GetY() * sT; m[9] = uyuz * oCT + u.GetX() * sT; m[10] = uzSq + cT * (1.0f - uzSq);  m[11] = 0.0f;
  m[12] = 0.0f;                      m[13] = 0.0f;                      m[14] = 0.0f;                       m[14] = 1.0f;

  return m;
}

Mat4 Mat4::Translate(const Vec3 &translation)
{
  return Translate(translation.GetX(), translation.GetY(), translation.GetZ());
}

Mat4 Mat4::Translate(f32 tX, f32 tY, f32 tZ)
{
  return Mat4
  {
    1.0f, 0.0f, 0.0f, tX,
    0.0f, 1.0f, 0.0f, tY,
    0.0f, 0.0f, 1.0f, tZ,
    0.0f, 0.0f, 0.0f, 1.0f
  };
}

f32 &Mat4::operator[](size_t index)
{
  return mMat[index];
}

f32 Mat4::operator[](size_t index) const
{
  return const_cast<Mat4 &>(*this)[index];
}

Mat4 Mat4::PerspectiveProjection(f32 fov, f32 width, f32 height, f32 zNear, f32 zFar)
{
  const auto aspect = width / height;
  const auto tanFOV = tan(fov * 0.5f);
  const auto nf = zNear - zFar;

  return Mat4
  {
    1.0f / (aspect * tanFOV), 0.0f,           0.0f,				             0.0f,
    0.0f,									    1.0f / tanFOV,  0.0f,		                 0.0f,
    0.0f,									    0.0f,				   (zFar + zNear) / nf,      2.0f * zNear * zFar / nf,
    0.0f,									    0.0f,          -1.0f,                    0.0f
  };
}

Mat4 Mat4::OrthographicProjection(f32 left, f32 right, f32 top, f32 bottom, f32 zNear, f32 zFar)
{
  const auto horizontal = right + left;
  const auto vertical = top + bottom;
  const auto width = right - left;
  const auto height = top - bottom;
  const auto fn = zFar - zNear;
  const auto nf = zFar + zNear;

  return Mat4
  {
    2.0f / width, 0.0f,          0.0f,        -horizontal / width,
    0.0f,         2.0f / height, 0.0f,        -vertical / height,
    0.0f,         0.0f,          -2.0f / fn,  -nf / fn,
    0.0f,         0.0f,          0.0f,        1.0f
  };
}

Mat4 Mat4::LookAt(const Vec3& eye, const Vec3& worldUp, const Vec3& target)
{
  auto back = (eye - target).Normalize();
  auto right = Cross(worldUp, back).Normalize();
  auto up = Cross(back, right).Normalize();

  return Mat4
  {
    right.GetX(),		      right.GetY(),         right.GetZ(),  -Dot(eye, right),
    up.GetX(),		        up.GetY(),            up.GetZ(),     -Dot(eye, up),
    back.GetX(),		      back.GetY(),          back.GetZ(),   -Dot(eye, back),
    0.0f,                 0.0f,                 0.0f,           1.0f
  };
}

Vec4 Mat4::GetRow(int r) const
{
  return Vec4(mMat[r], mMat[4 + r], mMat[8 + r], mMat[12 + r]);
}

Vec4 Mat4::GetCol(int c) const
{
  c *= 4;
  return Vec4(mMat[c], mMat[c + 1], mMat[c + 2], mMat[c + 3]);
}

Mat4 operator*(const Mat4 &lhs, const Mat4 &rhs)
{
  // Get the rows and columns and multiply them
  auto rowOne = lhs.GetRow(0);
  auto rowTwo = lhs.GetRow(1);
  auto rowThree = lhs.GetRow(2);
  auto rowFour = lhs.GetRow(3);

  auto colOne = rhs.GetCol(0);
  auto colTwo = rhs.GetCol(1);
  auto colThree = rhs.GetCol(2);
  auto colFour = rhs.GetCol(3);

  return Mat4
  {
    Dot(colOne, rowOne),   Dot(colTwo, rowOne),   Dot(colThree, rowOne),   Dot(colFour, rowOne),
    Dot(colOne, rowTwo),   Dot(colTwo, rowTwo),   Dot(colThree, rowTwo),   Dot(colFour, rowTwo),
    Dot(colOne, rowThree), Dot(colTwo, rowThree), Dot(colThree, rowThree), Dot(colFour, rowThree),
    Dot(colOne, rowFour),  Dot(colTwo, rowFour),  Dot(colThree, rowFour),  Dot(colFour, rowFour),
  };
}

Mat4 operator*(f32 scale, const Mat4 &rhs)
{
  auto m(rhs);
  for (auto &elem : m.mMat)
  {
    elem *= scale;
  }

  return m;
}
