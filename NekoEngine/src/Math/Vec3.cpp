#include "pch.h"

Vec3::Vec3(f32 x, f32 y, f32 z)
	: mX{ x }, mY{ y }, mZ{ z }
{
}

Vec3::Vec3(f32(&data)[3])
  : mX{ data[0] }, mY{ data[1] }, mZ{ data[2] }
{
}

f32 Vec3::GetX() const
{
	return mX;
}

f32 Vec3::GetY() const
{
	return mY;
}

f32 Vec3::GetZ() const
{
	return mZ;
}

void Vec3::SetX(f32 x)
{
  mX = x;
}

void Vec3::SetY(f32 y)
{
  mY = y;
}

void Vec3::SetZ(f32 z)
{
  mZ = z;
}

void Vec3::Set(f32 x, f32 y, f32 z)
{
  mX = x;
  mY = y;
  mZ = z;
}

f32 Vec3::Length() const
{
  return sqrt(SqLength());
}

f32 Vec3::SqLength() const
{
  return mX * mX + mY * mY + mZ * mZ;
}

Vec3 Vec3::Normalize() const
{
  const auto invSq = InvSqrt(SqLength());
  return Vec3(mX * invSq, mY * invSq, mZ * invSq);
}

Vec3 &Vec3::SetNormalize()
{
  const auto invSq = InvSqrt(SqLength());
  mX *= invSq;
  mY *= invSq;
  mZ *= invSq;

  return *this;
}

void Vec3::Add(f32 x, f32 y, f32 z)
{
  mX += x;
  mY += y;
  mZ += z;
}

void Vec3::Add(const Vec3 &rhs)
{
  mX += rhs.mX;
  mY += rhs.mY;
  mZ += rhs.mZ;
}

Vec3 &Vec3::operator+=(const Vec3 &rhs)
{
  mX += rhs.mX;
  mY += rhs.mY;
  mZ += rhs.mZ;

  return *this; 
}

Vec3 &Vec3::operator-=(const Vec3 &rhs)
{
  mX -= rhs.mX;
  mY -= rhs.mY;
  mZ -= rhs.mZ;
  return *this;
}

Vec3 &Vec3::operator*=(f32 scalar)
{
  mX *= scalar;
  mY *= scalar;
  mZ *= scalar;
  return *this;
}

Vec3 operator+(const Vec3 &lhs, const Vec3 &rhs)
{
	return Vec3(lhs.GetX() + rhs.GetX(), 
              lhs.GetY() + rhs.GetY(), 
              lhs.GetZ() + rhs.GetZ());
}

Vec3 operator-(const Vec3 & lhs, const Vec3 & rhs)
{
  return Vec3(lhs.GetX() - rhs.GetX(),
              lhs.GetY() - rhs.GetY(),
              lhs.GetZ() - rhs.GetZ());
}

Vec3 operator*(f32 scalar, const Vec3 &rhs)
{
  auto v(rhs);
  return v *= scalar;
}

Vec3 operator*(const Vec3 &lhs, f32 scalar)
{
  return operator*(scalar, lhs);
}

f32 Dot(const Vec3 & lhs, const Vec3 & rhs)
{
  return (lhs.GetX() * rhs.GetX() +
          lhs.GetY() * rhs.GetY() +
          lhs.GetZ() * rhs.GetZ());
}

Vec3 Cross(const Vec3 & lhs, const Vec3 & rhs)
{
  return Vec3
  {
    lhs.GetY() * rhs.GetZ() - rhs.GetY() * lhs.GetZ(),
    rhs.GetX() * lhs.GetZ() - lhs.GetX() * rhs.GetZ(),
    lhs.GetX() * rhs.GetY() - rhs.GetX() * lhs.GetY()
  };
}

std::ostream &operator<<(std::ostream &os, const Vec3 &v)
{
  return os << "{ x:" << v.GetX() << " y: " << v.GetY() << " z: " << v.GetZ() << " }";
}

const Vec3 Vec3::XRIGHT = Vec3(1.0f, 0.0f, 0.0f);
const Vec3 Vec3::YUP = Vec3(0.0f, 1.0f, 0.0f);
const Vec3 Vec3::ZFORWARD = Vec3(0.0f, 0.0f, 1.0f);