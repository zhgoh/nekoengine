#pragma once

class Mat4;

class Vec4
{
public:
  explicit Vec4(f32 x = 0.0f, f32 y = 0.0f, f32 z = 0.0f, f32 w = 0.0f);

  f32 GetX() const;
  f32 GetY() const;
  f32 GetZ() const;
  f32 GetW() const;

  void SetX(f32 x);
  void SetY(f32 y);
  void SetZ(f32 z);
  void SetW(f32 w);
  void Set(f32 x = 0.0f, f32 y = 0.0f, f32 z = 0.0f, f32 w = 0.0f) ;

  f32 Length() const;
  f32 SqLength() const;

  Vec4 Normalize() const;
  Vec4 &SetNormalize();

  Vec4 &operator+=(const Vec4 &rhs);
  Vec4 &operator-=(const Vec4 &rhs);


private:
  f32 mX;
  f32 mY;
  f32 mZ;
  f32 mW;
};

Vec4 operator+(const Vec4 &lhs, const Vec4 &rhs);
Vec4 operator-(const Vec4 &lhs, const Vec4 &rhs);
Vec4 operator*(f32 scalar,      const Vec4 &rhs);
Vec4 operator*(const Vec4 &lhs, f32 scalar);
Vec4 operator*(const Mat4 &lhs, const Vec4 &rhs);
f32  Dot(const Vec4 &lhs,       const Vec4 &rhs);

std::ostream &operator<<(std::ostream &os, const Vec4 &v);