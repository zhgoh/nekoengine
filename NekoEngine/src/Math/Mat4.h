#pragma once

class Quat;

class Mat4
{
public:
  Mat4();
  Mat4(f32 m00, f32 m01, f32 m02, f32 m03,
       f32 m10, f32 m11, f32 m12, f32 m13,
       f32 m20, f32 m21, f32 m22, f32 m23,
       f32 m30, f32 m31, f32 m32, f32 m33);
  explicit Mat4(const Quat &quat);

  Mat4 Inverse() const;
  Mat4 Transpose() const;
  const f32 *Data() const;

  f32 &operator[](size_t index);
  f32  operator[](size_t index) const;

  static Mat4 Scale(f32 scale);
  static Mat4 Scale(const Vec3 &scale);
  static Mat4 Scale(f32 scaleX, f32 scaleY, f32 scaleZ);
  static Mat4 Rotate(const Vec3 &axis, f32 angleRad);
  static Mat4 Translate(const Vec3 &translation);
  static Mat4 Translate(f32 tX, f32 tY, f32 tZ);

  static Mat4 PerspectiveProjection(f32 fov, f32 width, f32 height, f32 zNear, f32 zFar);
  static Mat4 OrthographicProjection(f32 left, f32 right, f32 top, f32 bottom, f32 zNear, f32 zFar);
  static Mat4 LookAt(const Vec3& eye, const Vec3& worldUp, const Vec3& target);

  Vec4 GetRow(int r) const;
  Vec4 GetCol(int c) const;

private:
  union {
    std::array<f32, 16> mMat;
    std::array<Vec4, 4> mVec;
  };

  friend Mat4 operator*(const Mat4 &lhs, const Mat4 &rhs);
  friend Mat4 operator*(f32 scale, const Mat4 &rhs);
};

Mat4 operator*(const Mat4 &lhs, const Mat4 &rhs); 
Mat4 operator*(f32 scale, const Mat4 &rhs);