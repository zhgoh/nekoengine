#pragma once

class Vec3;
class Vec2
{
public:
  explicit Vec2(f32 x = 0.0f, f32 y = 0.0f);
  explicit Vec2(f32(&data)[2]);
  explicit Vec2(const Vec3 &v);

  f32 GetX() const;
  f32 GetY() const;
  void SetX(f32 x);
  void SetY(f32 y);
  void Set(f32 x = 0.0f, f32 y = 0.0f);

  f32 Length() const;
  f32 SqLength() const;

  Vec2 Normalize() const;
  Vec2 &SetNormalize();

private:
  f32 mX;
  f32 mY;
};

std::ostream &operator<<(std::ostream &os, const Vec2 &v);

Vec2 operator+(const Vec2 &lhs, const Vec2 &rhs);
Vec2 operator-(const Vec2 &lhs, const Vec2 &rhs);
Vec2 operator*(f32 scalar,      const Vec2 &rhs);
Vec2 operator*(const Vec2 &lhs, f32 scalar);
f32 Dot(const Vec2 &lhs, const Vec2 &rhs);