#include "pch.h"

Quat::Quat(f32 x, f32 y, f32 z, f32 w) :
  mX(x), mY(y), mZ(z), mW(w)
{
}

f32 Quat::GetX() const
{
  return mX;
}

f32 Quat::GetY() const
{
  return mY;
}

f32 Quat::GetZ() const
{
  return mZ;
}

f32 Quat::GetW() const
{
  return mW;
}

void Quat::SetX(f32 x)
{
  mX = x;
}

void Quat::SetY(f32 y)
{
  mY = y;
}

void Quat::SetZ(f32 z)
{
  mZ = z;
}

void Quat::SetW(f32 w)
{
  mW = w;
}

f32 Quat::Length() const
{
  return sqrt(SqLength());
}

f32 Quat::SqLength() const
{
  return mW * mW + mX * mX + mY * mY + mZ * mZ;
}

Quat Quat::Normalize() const
{
  const auto invSq = InvSqrt(SqLength());
  return Quat(mW * invSq, mX * invSq, mY * invSq, mZ * invSq);
}

Quat &Quat::SetNormalize()
{
  const auto invSq = InvSqrt(SqLength());
  mX *= invSq;
  mY *= invSq;
  mZ *= invSq;
  mW *= invSq;

  return *this;
}

void Quat::SetIdentity()
{
  mW = 1.0f;
  mX = mY = mZ = 0.0f;
}

Quat &Quat::operator+=(const Quat &rhs)
{
  mW += rhs.GetW();
  mX += rhs.GetX();
  mY += rhs.GetY();
  mZ += rhs.GetZ();

  return *this;
}

Quat & Quat::operator-=(const Quat &rhs)
{
  mW -= rhs.GetW();
  mX -= rhs.GetX();
  mY -= rhs.GetY();
  mZ -= rhs.GetZ();

  return *this;
}

Quat Quat::Slerp(const Quat &a, const Quat &b, float t)
{
  assert(t >= 0);
  assert(t <= 1);

  auto flip = 1.0f;
  auto cosine = a.GetW() * b.GetW() + a.GetX() * b.GetX() + a.GetY() * b.GetY() + a.GetZ() * b.GetZ();

  if (cosine < 0.0f)
  {
    cosine = -cosine;
    flip = -1.0f;
  }

  if (1.0f - cosine < FLT_EPSILON)
    return a * (1.0f - t) + b * (t * flip);

  auto theta = acos(cosine);
  auto sine  = sin(theta);
  auto beta  = sin((1.0f - t) * theta) / sine;
  auto alpha = sin(t*theta) / sine * flip;

  return a * beta + b * alpha;
}

std::ostream &operator<<(std::ostream &os, const Quat &q)
{
  return os << "{ w: " << q.GetW() << " x: " << q.GetX() << " y: " << q.GetY() << " z: " << q.GetZ() << " }";
}

Quat operator+(const Quat &lhs, const Quat &rhs)
{
  return Quat(lhs.GetW() + rhs.GetW(),
              lhs.GetX() + rhs.GetX(),
              lhs.GetY() + rhs.GetY(),
              lhs.GetZ() + rhs.GetZ());
}

Quat operator-(const Quat &lhs, const Quat &rhs)
{
  return Quat(lhs.GetW() - rhs.GetW(),
              lhs.GetX() - rhs.GetX(),
              lhs.GetY() - rhs.GetY(),
              lhs.GetZ() - rhs.GetZ());
}

Quat operator*(f32 scalar, const Quat &rhs)
{
  return Quat(scalar * rhs.GetW(),
              scalar * rhs.GetX(),
              scalar * rhs.GetY(),
              scalar * rhs.GetZ());
}

Quat operator*(const Quat &lhs, f32 scalar)
{
  return operator*(scalar, lhs);
}

Quat operator*(const Quat &lhs, const Quat &rhs)
{
  return Quat(lhs.GetW()*rhs.GetW() - lhs.GetX()*rhs.GetX() - lhs.GetY()*rhs.GetY() - lhs.GetZ()*rhs.GetZ(),
              lhs.GetW()*rhs.GetX() + lhs.GetX()*rhs.GetW() + lhs.GetY()*rhs.GetZ() - lhs.GetZ()*rhs.GetY(),
              lhs.GetW()*rhs.GetY() - lhs.GetX()*rhs.GetZ() + lhs.GetY()*rhs.GetW() + lhs.GetZ()*rhs.GetX(),
              lhs.GetW()*rhs.GetZ() + lhs.GetX()*rhs.GetY() - lhs.GetY()*rhs.GetX() + lhs.GetZ()*rhs.GetW());
}