#pragma once
struct Texture
{
  char textureName[MAX_STR_LEN];
  u32 textureID;
};

Texture       &TextureLoad(cstr fileName);
const Texture &TextureGet(cstr fileName);