#pragma once

void SpriteAdd(Handle hdl, cstr name);
void SpriteRemove(Handle hdl);

void SpriteBatchInit(cstr atlas, f32 imgWidth, f32 imgHeight);
void SpriteBatchSet(cstr name, f32 imgX, f32 imgY, f32 imgWidth, f32 imgHeight);
void SpriteBatchRender(f32 interpolation);
void SpriteBatchUnload();