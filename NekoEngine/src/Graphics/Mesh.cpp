#include "pch.h"
INTERNAL Array<Mesh, MAX_MESH> meshLibrary;

INTERNAL
void MeshGen(Mesh &mesh,
             const std::vector<Vec3> &vertices,
             const std::vector<u32> &vertexIndices,
             const std::vector<Vec3> &normals,
             const std::vector<u32> &normalIndices,
             const std::vector<Vec2> &uvs,
             const std::vector<u32> &uvIndices)
{
  glGenVertexArrays(1, &mesh.vao);
  glGenBuffers(1, &mesh.vbo[POS]);
  glGenBuffers(1, &mesh.ebo);
  glGenBuffers(1, &mesh.vbo[NORMAL]);
  glGenBuffers(1, &mesh.vbo[UV]);

  // 1. Bind Vertex Array Object
  glBindVertexArray(mesh.vao);
  {
    // 2. Copy our vertices array in a vertex buffer for OpenGL to use
    glBindBuffer(GL_ARRAY_BUFFER, mesh.vbo[POS]);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof vertices[0], vertices.data(), GL_STATIC_DRAW);

    // 3. Copy our index array in a element buffer for OpenGL to use
    // Why not bind to GL_ELEMENT_ARRAY_BUFFER?
    // Because binding to GL_ELEMENT_ARRAY_BUFFER attaches the EBO to the currently bound VAO, which might stomp somebody else's state.
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, vertexIndices.size() * sizeof vertexIndices[0], vertexIndices.data(), GL_STATIC_DRAW);

    // 4. Then set the vertex attributes pointers
    glVertexAttribPointer(POS, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(POS);

    glBindBuffer(GL_ARRAY_BUFFER, mesh.vbo[NORMAL]);
    glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof normals[0], normals.data(), GL_STATIC_DRAW);

    glVertexAttribPointer(NORMAL, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(NORMAL);

    glBindBuffer(GL_ARRAY_BUFFER, mesh.vbo[UV]);
    glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof uvs[0], uvs.data(), GL_STATIC_DRAW);

    glVertexAttribPointer(UV, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(UV);
  }
  // 5. Unbind VAO (NOT the EBO)
  glBindVertexArray(0);
}

const Mesh &MeshLoad(cstr fileName)
{
  Log("Please wait. Loading mesh", fileName);

  char path[MAX_STR_LEN];
  ne_strcpy(path, MESH, fileName, MESH_EXT);

  meshLibrary.increment();
  auto &currentMesh = meshLibrary.last();
  currentMesh.instanceID = meshLibrary.size() - 1;
  strcpy_s(currentMesh.name, fileName);

  std::vector<u32> vertexIndices, uvIndices, normalIndices;
  std::vector<Vec3> vertices;
  std::vector<Vec2> temp_uvs;
  std::vector<Vec3> temp_normals;

  FILE *file;
  auto err = fopen_s(&file, path, "rt");

  if (err)
  {
    LogError("Impossible to open the file !");
    assert(false);
  }

  while (true)
  {
    char lineHeader[1028];
    auto res = fscanf_s(file, "%s", lineHeader, _countof(lineHeader));
    if (res == EOF)
    {
      break;
    }

    if (strcmp(lineHeader, "v") == 0)
    {
      float vertex[3];
      fscanf_s(file, "%f %f %f\n", &vertex[0], &vertex[1], &vertex[2]);
      vertices.push_back(Vec3(vertex));
    }
    else if (strcmp(lineHeader, "vt") == 0)
    {
      float uv[2];
      fscanf_s(file, "%f %f\n", &uv[0], &uv[1]);
      temp_uvs.push_back(Vec2(uv));
    }
    else if (strcmp(lineHeader, "vn") == 0)
    {
      float normal[3];
      fscanf_s(file, "%f %f %f\n", &normal[0], &normal[1], &normal[2]);
      temp_normals.push_back(Vec3(normal));
    }
    else if (strcmp(lineHeader, "f") == 0)
    {
      u32 vertexIndex[3], uvIndex[3], normalIndex[3];
      auto matches = fscanf_s(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", 
                              &vertexIndex[0], 
                              &uvIndex[0], 
                              &normalIndex[0], 
                              &vertexIndex[1], 
                              &uvIndex[1], 
                              &normalIndex[1], 
                              &vertexIndex[2], 
                              &uvIndex[2], 
                              &normalIndex[2]);
      if (matches != 9)
      {
        LogError("File can't be read by our simple parser : ( Try exporting with other options");
        assert(false);
      }

      vertexIndices.push_back(vertexIndex[0]);
      vertexIndices.push_back(vertexIndex[1]);
      vertexIndices.push_back(vertexIndex[2]);

      uvIndices.push_back(uvIndex[0]);
      uvIndices.push_back(uvIndex[1]);
      uvIndices.push_back(uvIndex[2]);

      normalIndices.push_back(normalIndex[0]);
      normalIndices.push_back(normalIndex[1]);
      normalIndices.push_back(normalIndex[2]);
    }
    else 
    {
      // Probably a comment, eat up the rest of the line
      char stupidBuffer[1000];
      fgets(stupidBuffer, 1000, file);
    }
  }

  fclose(file);

  std::vector<Vec2> uvs;
  std::vector<Vec3> normals;

  // For each vertex of each triangle
  for (u32 i = 0; i < vertexIndices.size(); ++i)
  {
    //auto vertexIndex = vertexIndices[i];
    auto uvIndex = uvIndices[i];
    auto normalIndex = normalIndices[i];

    auto &uv = temp_uvs[uvIndex - 1];
    auto &normal = temp_normals[normalIndex - 1];

    uvs.push_back(uv);
    normals.push_back(normal);
    --vertexIndices[i];
  }

  currentMesh.indexCount = vertexIndices.size();
  MeshGen(currentMesh, 
          vertices, vertexIndices, 
          normals, normalIndices,
          uvs, uvIndices);

  return currentMesh;
}

void MeshUnload()
{
  for (auto &elem : meshLibrary)
  {
    for (auto &bo : elem.vbo)
    {
      glDeleteBuffers(1, &bo);
      bo = 0;
    }
    
    glDeleteBuffers(1, &elem.ebo);
    glDeleteVertexArrays(1, &elem.vao);

    elem.vao = elem.ebo = 0;
  }
}

const Mesh &MeshGet(cstr fileName)
{
  for (auto &elem : meshLibrary)
  {
    if (!strcmp(fileName, elem.name))
    {
      return elem;
    }
  }

  LogError("Mesh not found!");
  assert(false);
  return meshLibrary[0];
}
