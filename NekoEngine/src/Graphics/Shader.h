#pragma once

struct Shader
{
  GLuint programID;
  char name[MAX_STR_LEN];
  std::unordered_map<std::string, GLuint> shaderLoc;
};

const Shader  &ShaderGet(cstr shaderName);
Shader        &ShaderLoad(cstr shaderName, cstr vsFileName, cstr fsFileName);
void          ShaderGetUniform(Shader &shader, cstr uniformName);
void          ShaderSetUniform(Shader &shader, cstr uniformName, Vec3 &v);
void          ShaderSetUniform(Shader &shader, cstr uniformName, Mat4 &m);