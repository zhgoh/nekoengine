#pragma once

void RendererInit(HWND hWnd);
void RendererShutDown();
void RendererDraw(f32 interpolation);
void RendererResize(i32 width, i32 height);