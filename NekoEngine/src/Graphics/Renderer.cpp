#include "pch.h"

#pragma comment(lib, "opengl32.lib")

#define MAX_VERTEX_MEMORY 512 * 1024
#define MAX_ELEMENT_MEMORY 128 * 1024

INTERNAL HDC hdc;

void RendererInit(HWND hWnd)
{
  PIXELFORMATDESCRIPTOR pfd =
  {
    sizeof(PIXELFORMATDESCRIPTOR),
    1,
    PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    //Flags
    PFD_TYPE_RGBA,            //The kind of framebuffer. RGBA or palette.
    32,                        //Colordepth of the framebuffer.
    0, 0, 0, 0, 0, 0,
    0,
    0,
    0,
    0, 0, 0, 0,
    24,                        //Number of bits for the depthbuffer
    8,                        //Number of bits for the stencilbuffer
    0,                        //Number of Aux buffers in the framebuffer.
    PFD_MAIN_PLANE,
    0,
    0, 0, 0
  };

  hdc = GetDC(hWnd);

  auto winPFD = ChoosePixelFormat(hdc, &pfd);
  SetPixelFormat(hdc, winPFD, &pfd);

  auto glRC = wglCreateContext(hdc);
  wglMakeCurrent(hdc, glRC);

  if (gl3wInit())
  {
    LogError("Cannot Init GL3W");
    assert(false);
  }

  if (!gl3wIsSupported(3, 2))
  {
    LogError("OpenGL 3.2 not supported");
    assert(false);
  }

  RendererResize(GetWindowWidth(), GetWindowHeight());
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  ImGui_Init(hWnd);
}

void RendererShutDown()
{
  ImGui_ShutDown();
}

INTERNAL
void RendererShowUI()
{
  ImGui_NewFrame();

  ImGui::Begin("Test");
  ImGui::Button("aaa");
  ImGui::End();
}

void RendererClearFrame()
{
  //static const GLfloat color[] = { 0.392156899f, 0.584313750f, 0.929411829f, 1.0f };
  //glClearBufferfv(GL_COLOR, 0, color);
  //glClearColor(0.392156899f, 0.584313750f, 0.929411829f, 1.0f);
  glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void RendererDrawFrame(f32 interpolation)
{
  glEnable(GL_DEPTH_TEST);
  // cull back-faced or clockwise oriented primitives
  glEnable(GL_CULL_FACE);
  glFrontFace(GL_CCW);
  glCullFace(GL_BACK);

  DisplayListRender(interpolation);
  SpriteBatchRender(interpolation);
}

void RendererDraw(f32 interpolation)
{
  RendererShowUI();
  RendererClearFrame();
  RendererDrawFrame(interpolation);
  ImGui::Render();
  SwapBuffers(hdc);
}

void RendererResize(i32 width, i32 height)
{
  glViewport(0, 0, width, height);
}
