#include "pch.h"

INTERNAL Array<Shader, MAX_SHADER> shaderLibrary;

INTERNAL
std::string ShaderRead(cstr shaderPath)
{
  std::string shaderCode;
  std::ifstream ifs(shaderPath);
  if (ifs.is_open())
  {
    std::string line;
    while (getline(ifs, line))
    {
      shaderCode += "\n" + line;
    }
  }
  else
  {
    LogError("Error opening shader file: ", shaderPath);
  }

  return shaderCode;
}

INTERNAL
void ShaderCompile(const std::string &shaderCode, GLuint shaderID)
{
  // Compile shader
  auto shader = shaderCode.c_str();
  glShaderSource(shaderID, 1, &shader, nullptr);
  glCompileShader(shaderID);

  // Check the shader
  auto result = GL_FALSE;
  GLint infoLogLength;

  glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
  if (result == GL_FALSE)
  {
    glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (infoLogLength > 0)
    {
      std::vector<char> shaderErrorMessage(infoLogLength + 1);
      glGetShaderInfoLog(shaderID, infoLogLength, nullptr, &shaderErrorMessage[0]);
      LogError(&shaderErrorMessage[0]);
    }
  }
}

INTERNAL
GLuint ShaderLink(GLuint vsID, GLuint fsID)
{
  // Create and link the shaders
  auto programID = glCreateProgram();
  glAttachShader(programID, vsID);
  glAttachShader(programID, fsID);
  glLinkProgram(programID);

  // Check the program
  auto result = GL_FALSE;
  GLint infoLogLength;

  glGetProgramiv(programID, GL_LINK_STATUS, &result);
  if (result == GL_FALSE)
  {
    glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (infoLogLength > 0)
    {
      std::vector<char> programErrorMessage(infoLogLength + 1);
      glGetProgramInfoLog(programID, infoLogLength, nullptr, &programErrorMessage[0]);
      LogError(&programErrorMessage[0]);
    }
  }

  glDetachShader(programID, vsID);
  glDetachShader(programID, fsID);

  glDeleteShader(vsID);
  glDeleteShader(fsID);

  return programID;
}

const Shader &ShaderGet(cstr shaderName)
{
  for (auto &elem : shaderLibrary)
  {
    if (strcmp(elem.name, shaderName) == 0)
    {
      return elem;
    }
  }

  LogError("No shader found!");
  assert(false);
  return shaderLibrary[0];
}

Shader &ShaderLoad(cstr shaderName, cstr vsFileName, cstr fsFileName)
{
  char vsPath[MAX_STR_LEN];
  char fsPath[MAX_STR_LEN];
  ne_strcpy(vsPath, SHADER, vsFileName, VS_EXT);
  ne_strcpy(fsPath, SHADER, fsFileName, FS_EXT);

  shaderLibrary.increment();
  auto &shader = shaderLibrary.last();

  ne_strcpy(shader.name, shaderName);

  // First create the shaders
  auto vsID = glCreateShader(GL_VERTEX_SHADER);
  auto fsID = glCreateShader(GL_FRAGMENT_SHADER);

  // Read the shader file
  auto vsShader = ShaderRead(vsPath);
  auto fsShader = ShaderRead(fsPath);

  // Compile
  ShaderCompile(vsShader, vsID);
  ShaderCompile(fsShader, fsID);

  // Link shaders
  shader.programID = ShaderLink(vsID, fsID);
  return shader;
}

void ShaderGetUniform(Shader & shader, cstr uniformName)
{
  shader.shaderLoc[uniformName] = glGetUniformLocation(shader.programID, uniformName);
}

void ShaderSetUniform(Shader &shader, cstr uniformName, Vec3 &v)
{
  glUniform3f(shader.shaderLoc[uniformName], v.GetX(), v.GetY(), v.GetZ());
}

void ShaderSetUniform(Shader &shader, cstr uniformName, Mat4 &m)
{
  glUniformMatrix4fv(shader.shaderLoc[uniformName], 1, GL_FALSE, m.Data());
}
