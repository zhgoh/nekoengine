#include "pch.h"

struct Sprites
{
  Array<Vec3,   MAX_SPRITE * 6> verts;
  Array<Vec2,   MAX_SPRITE * 6> uvs;
  Array<u32,    MAX_SPRITE> transformIndex;
  Array<Handle, MAX_ENTITY> entityHdl;

  const Array<Vec3, MAX_SPRITE> *translation;
  const Array<Vec3, MAX_SPRITE> *rotation;
  const Array<Vec3, MAX_SPRITE> *scale;
};

struct Atlas
{
  GLuint vao          = 0;
  GLuint vbo[2]       = { 0, 0 };
  GLuint textureID    = 0;
  Shader shader;

  f32 width;
  f32 height;
};

struct SpriteBatch
{
  char name[MAX_STR_LEN];
  Array<Vec2, 6> uv;
};

INTERNAL Sprites sprites;
INTERNAL Atlas atlas;

INTERNAL u32 numObjects = 0;
INTERNAL Array<SpriteBatch, MAX_SPRITE> spriteBatchLibrary;

INTERNAL
void SpriteBatchClearBuffer()
{
  sprites.verts.clear();
}

INTERNAL 
void SpriteBatchAddVert(const Vec3 &trans, const Vec2 &scale, f32 angleRad)
{
  const auto tx = trans.GetX();
  const auto ty = trans.GetY();
  const auto sx = scale.GetX();
  const auto sy = scale.GetY();

  const auto scaleMat  = Mat4::Scale(Vec3(sx, sy, 1.0f));
  const auto rotMat    = Mat4::Rotate(Vec3::ZFORWARD, angleRad);
  const auto transMat  = Mat4::Translate(tx, ty, 0.0f);

  auto model  = scaleMat * rotMat * transMat;
  auto bl4    = model * Vec4(-0.5f, -0.5f, 0.0f, 1.0f);
  auto tl4    = model * Vec4(-0.5f, 0.5f, 0.0f, 1.0f);
  auto br4    = model * Vec4(0.5f, -0.5f, 0.0f, 1.0f);
  auto tr4    = model * Vec4(0.5f, 0.5f, 0.0f, 1.0f);

  auto bl = Vec3(bl4.GetX(), bl4.GetY(), 0.0f);
  auto tl = Vec3(tl4.GetX(), tl4.GetY(), 0.0f);
  auto br = Vec3(br4.GetX(), br4.GetY(), 0.0f);
  auto tr = Vec3(tr4.GetX(), tr4.GetY(), 0.0f);

  sprites.verts.push(br);
  sprites.verts.push(tl);
  sprites.verts.push(bl);
  sprites.verts.push(br);
  sprites.verts.push(tr);
  sprites.verts.push(tl);
}

void SpriteBatchRegister(
  const Array<Vec3, MAX_ENTITY> &translation, 
  const Array<Vec3, MAX_ENTITY> &rotation, 
  const Array<Vec3, MAX_ENTITY> &scale)
{
  sprites.translation = &translation;
  sprites.rotation    = &rotation;
  sprites.scale       = &scale;
}

void SpriteBatchInit(cstr atlasName, f32 imgWidth, f32 imgHeight)
{
  char textureName[MAX_STR_LEN];
  ne_strcpy(textureName, TEXTURE, atlasName, TEXTURE_EXT);

  atlas.width = imgWidth;
  atlas.height = imgHeight;

  /* load an image file directly as a new OpenGL texture */
  atlas.textureID = SOIL_load_OGL_texture
  (
    textureName,
    SOIL_LOAD_AUTO,
    SOIL_CREATE_NEW_ID,
    SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
  );

  /* check for an error during the load process */
  if (atlas.textureID == 0)
  {
    LogError("SOIL loading error:", SOIL_last_result());
    assert(false);
  }

  glGenVertexArrays(1, &atlas.vao);
  glGenBuffers(2, atlas.vbo);

  glBindVertexArray(atlas.vao);
  {
    glBindBuffer(GL_ARRAY_BUFFER, atlas.vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, MAX_SPRITE * sizeof(Vec3), nullptr, GL_DYNAMIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    
    glBindBuffer(GL_ARRAY_BUFFER, atlas.vbo[1]);
    glBufferData(GL_ARRAY_BUFFER, MAX_SPRITE * sizeof(Vec2), nullptr, GL_DYNAMIC_DRAW);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
  }
  glBindVertexArray(0);

  atlas.shader = ShaderLoad("Sprite", "Sprite", "Sprite");
}

void SpriteBatchSet(cstr name, f32 imgX, f32 imgY, f32 imgWidth, f32 imgHeight)
{
  assert(imgX + imgWidth <= atlas.width);
  assert(imgY + imgHeight <= atlas.height);

  spriteBatchLibrary.increment();
  auto &spriteBatch = spriteBatchLibrary.last();
  ne_strcpy(spriteBatch.name, name);

  const auto minX = imgX;
  const auto maxX = imgX + imgWidth;
  const auto minY = imgY;
  const auto maxY = imgY + imgHeight;

  spriteBatch.uv.resize(6);

  spriteBatch.uv[0].Set(maxX / atlas.width, minY / atlas.height);  // Bottom Right
  spriteBatch.uv[1].Set(minX / atlas.width, maxY / atlas.height);  // Top Left
  spriteBatch.uv[2].Set(minX / atlas.width, minY / atlas.height);  // Bottom Left

  spriteBatch.uv[3].Set(maxX / atlas.width, minY / atlas.height);  // Bottom Right
  spriteBatch.uv[4].Set(maxX / atlas.width, maxY / atlas.height);  // Top Right
  spriteBatch.uv[5].Set(minX / atlas.width, maxY / atlas.height);  // Top Left
}

void SpriteRemove(Handle hdl)
{
  HandleGetData(hdl);

  auto id = sprites.entityHdl.remove(hdl);
  assert(id != -1);

  --numObjects;
  sprites.transformIndex.removeAt(id);
  sprites.uvs.shift(id, 6);
}

void SpriteAdd(Handle hdl, cstr name)
{
  HandleGetData(hdl);
  sprites.entityHdl.push(hdl);

  ++numObjects;

  auto transformIndex = TransformGetIndex(hdl);
  sprites.transformIndex.push(transformIndex);

  for (auto &elem : spriteBatchLibrary)
  {
    if (strcmp(elem.name, name) == 0)
    {
      for (auto &uv : elem.uv)
      {
        sprites.uvs.push(uv);
      }
      break;
    }
  }
}

void SpriteBatchRender(f32 interpolation)
{
  static auto nearPlane = 0.04f;
  static auto farPlane = 1000.0f;
  
  static const auto halfWinWidth = static_cast<f32>(GetWindowWidth() >> 1);
  static const auto halfWinHeight = static_cast<f32>(GetWindowHeight() >> 1);

  static const auto projMat = Mat4::OrthographicProjection(-halfWinWidth, halfWinWidth, halfWinHeight, -halfWinHeight, nearPlane, farPlane);
  static const auto viewMat = Mat4::LookAt(Vec3(0.0f, 0.0f, 1.0f), Vec3::YUP, Vec3());
  static const auto vpMat = projMat * viewMat;

  for (u32 i = 0; i < numObjects; ++i)
  {
    auto id = sprites.transformIndex[i];
    auto &trans = (*sprites.translation)[id];
    auto &rot   = (*sprites.rotation)[id];
    auto &scale = (*sprites.scale)[id];

    SpriteBatchAddVert(trans, Vec2(scale), rot.GetZ());
  }

  glBindTexture(GL_TEXTURE_2D, atlas.textureID);

  glBindVertexArray(atlas.vao);
  glUseProgram(atlas.shader.programID);
  glUniformMatrix4fv(0, 1, GL_FALSE, vpMat.Data());

  glBindBuffer(GL_ARRAY_BUFFER, atlas.vbo[0]);
  glBufferSubData(GL_ARRAY_BUFFER, 0, MAX_SPRITE * sizeof(Vec3), sprites.verts.data());

  glBindBuffer(GL_ARRAY_BUFFER, atlas.vbo[1]);
  glBufferSubData(GL_ARRAY_BUFFER, 0, MAX_SPRITE * sizeof(Vec2), sprites.uvs.data());

  glDrawArrays(GL_TRIANGLES, 0, sprites.verts.size());

  glUseProgram(0);
  glBindVertexArray(0);
  glBindTexture(GL_TEXTURE_2D, 0);

  SpriteBatchClearBuffer();
}

void SpriteBatchUnload()
{
  glDeleteBuffers(2, atlas.vbo);
  glDeleteVertexArrays(1, &atlas.vao);

  atlas.vbo[0] = atlas.vbo[1] = 0;
  atlas.vao = 0;
}
