#pragma once

enum BufferObject
{
  POS,
  NORMAL,
  UV,
  MAX
};

struct Mesh
{
  i32 instanceID;
  char name[MAX_STR_LEN];
  GLuint vao;
  GLuint vbo[MAX];
  GLuint ebo;

  u32 indexCount;
};

const Mesh &MeshLoad(cstr fileName);
void        MeshUnload();
const Mesh &MeshGet(cstr fileName);