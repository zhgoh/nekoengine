#pragma once

IMGUI_API bool        ImGui_Init(HWND hWnd);
IMGUI_API void        ImGui_ShutDown();
IMGUI_API void        ImGui_NewFrame();

// Use if you want to reset your rendering device without losing ImGui state.
IMGUI_API void        ImGui_InvalidateDeviceObjects();
IMGUI_API bool        ImGui_CreateDeviceObjects();

/*
 * put this in WndProc Handler
 * if (ImGui_WndProcHandler()) return 1;
 */
IMGUI_API LRESULT ImGui_WndProcHandler(HWND, UINT msg, WPARAM wParam, LPARAM lParam);

namespace ImGui
{
  bool Combo(const char *label,   int *currIndex, std::vector<std::string>& values);
  bool ListBox(const char *label, int *currIndex, std::vector<std::string>& values);
}