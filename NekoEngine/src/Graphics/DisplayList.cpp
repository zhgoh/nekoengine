#include "pch.h"

struct DisplayList
{
  Array<GLuint, MAX_ENTITY> meshVAO;
  Array<GLuint, MAX_ENTITY> positionVBO;
  Array<GLuint, MAX_ENTITY> normalVBO;
  Array<GLuint, MAX_ENTITY> texcoordVBO;
  Array<GLuint, MAX_ENTITY> meshEBO;
  Array<GLuint, MAX_ENTITY> textures;
  Array<Shader, MAX_ENTITY> shaders;
  Array<u32,    MAX_ENTITY> indicesCount;
  Array<Mat4,   MAX_ENTITY> matrices;
  Array<Handle, MAX_ENTITY> entityHdl;
};

INTERNAL u32                        numObjects = 0;
INTERNAL DisplayList                displayList;

void DisplayListAdd(Handle hdl, const Mesh &mesh, const Shader &shader, const Texture &texture)
{
  HandleGetData(hdl);
  displayList.entityHdl.push(hdl);

  ++numObjects;
  displayList.meshVAO.push(mesh.vao);
  displayList.positionVBO.push(mesh.vbo[POS]);
  displayList.normalVBO.push(mesh.vbo[NORMAL]);
  displayList.texcoordVBO.push(mesh.vbo[UV]);
  displayList.meshEBO.push(mesh.ebo);
  displayList.textures.push(texture.textureID);
  displayList.shaders.push(shader);
  displayList.indicesCount.push(mesh.indexCount);
  displayList.matrices.increment();
}

void DisplayListRemove(Handle hdl)
{
  HandleGetData(hdl);

  auto id = displayList.entityHdl.remove(hdl);
  assert(id != -1);

  --numObjects;
  displayList.meshVAO.removeAt(id);
  displayList.positionVBO.removeAt(id);
  displayList.normalVBO.removeAt(id);
  displayList.texcoordVBO.removeAt(id);
  displayList.meshEBO.removeAt(id);
  displayList.textures.removeAt(id);
  displayList.shaders.removeAt(id);
  displayList.indicesCount.removeAt(id);
  displayList.matrices.removeAt(id);
}

void DisplayListRender(f32 /*interpolation*/)
{
  //static auto angle = 0.1f;
  static const auto PI = atan(1.0f) * 4.0f;

  static auto fov = PI * 0.3f;
  static auto nearPlane = 0.04f;
  static auto farPlane = 1000.0f;

  // Compute perps transform
  static const auto winWidth = static_cast<f32>(GetWindowWidth());
  static const auto winHeight = static_cast<f32>(GetWindowHeight());

  static const auto projMat = Mat4::PerspectiveProjection(fov, winWidth, winHeight, nearPlane, farPlane);
  static const auto viewMat = Mat4::LookAt(Vec3(0.0f, 10.0f, 10.0f), Vec3::YUP, Vec3());
  static const auto concatMat = projMat * viewMat;

  //TODO: Note that if an entity does not have a graphics, the index would not tally anymore
  const auto& transMatrix = TransformGetMatrix();
  for (u32 i = 0; i < displayList.matrices.size(); ++i)
  {
    displayList.matrices[i] = concatMat * transMatrix[i];
  }

  for (u32 i = 0; i < numObjects; ++i)
  {
    glBindTexture(GL_TEXTURE_2D, displayList.textures[i]); 

    glBindVertexArray(displayList.meshVAO[i]);
    const auto &shader = displayList.shaders[i];
    glUseProgram(shader.programID);

    //TODO: Use shader set uniform instead
    glUniformMatrix4fv(0, 1, GL_FALSE, displayList.matrices[i].Data());

    glDrawElements(GL_TRIANGLES, displayList.indicesCount[i], GL_UNSIGNED_INT, nullptr);
    glUseProgram(0);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
  }
}

void DisplayListClear()
{
  numObjects = 0;

  displayList.meshVAO.clear();
  displayList.positionVBO.clear();
  displayList.normalVBO.clear();
  displayList.texcoordVBO.clear();
  displayList.meshEBO.clear();
  displayList.textures.clear();
  displayList.shaders.clear();
  displayList.indicesCount.clear();
  displayList.matrices.clear();
  displayList.entityHdl.clear();
}