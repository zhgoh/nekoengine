#pragma once

void DisplayListAdd(Handle hdl, const Mesh &mesh, const Shader &shader, const Texture &texture);
void DisplayListRemove(Handle hdl);
void DisplayListRender(f32 interpolation);
void DisplayListClear();