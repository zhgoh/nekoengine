#include "pch.h"

INTERNAL Array<Texture, MAX_TEXTURE> textureLibrary;

Texture &TextureLoad(cstr fileName)
{
  char textureName[MAX_STR_LEN];
  ne_strcpy(textureName, TEXTURE, fileName, TEXTURE_EXT);

  textureLibrary.increment();
  auto &texture = textureLibrary.last();

  /* load an image file directly as a new OpenGL texture */
  ne_strcpy(texture.textureName, fileName);
  texture.textureID = SOIL_load_OGL_texture
  (
    textureName,
    SOIL_LOAD_AUTO,
    SOIL_CREATE_NEW_ID,
    SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
  );

  /* check for an error during the load process */
  if (texture.textureID == 0)
  {
    LogError("SOIL loading error:", SOIL_last_result());
    assert(false);
  }

  return texture;
}

const Texture &TextureGet(cstr fileName)
{
  for (auto &elem : textureLibrary)
  {
    if (strcmp(fileName, elem.textureName) == 0)
    {
      return elem;
    }
  }

  LogError("Cannot find textures");
  assert(false);
  return textureLibrary[0];
}
